@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/ak04-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h3>FR.AK.04. BANDING ASESMEN </h3>
                                        <table border="1" class="table">
                                            <tr class="coba1">
                                                <td style="border: 1px solid gray;"><label for="namaasesi">Nama Asesi:</label></td>
                                                <td style="border: 1px solid gray;" colspan="6" id="el-namaasesi"><input type="text" name="namaasesi" size="48" class="form-control" id="namaasesi" placeholder="masukan nama asesi"></td>

                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;"><label for="namaasesor">Nama Asesor:</label></td>
                                                <td style="border: 1px solid gray;" colspan="6"><input type="text" name="namaasesor" class="form-control" size="48" id="namaasesor" disabled></td>
                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;"><label for="tanggal">Tanggal Asesmen:</label></td>
                                                <td style="border: 1px solid gray;" colspan="6"><input type="date" size="48" name="tanggal" class="form-control" id="tanggal" disabled></td>
                                            </tr>

                                            <tr>
                                                <td colspan="6" style="text-align:center;">Jawablah dengan Ya atau tidak pernyataan-pernytaan berikut ini</td>
                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;">Apakah Proses Banding telah dijelaskan kepada Anda?</td>
                                                <td style="border: 1px solid gray;" colspan="3" style="text-align:center; ">
                                                    <select class="input form-select" name="prosesbanding" id="prosesbanding">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>

                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;" colspan="">Apakah Anda telah mendiskusikan Banding dengan Asesor</td>
                                                <td style="border: 1px solid gray;" colspan="3" style="text-align:center; ">
                                                    <select class="input form-select" name="diskusibanding" id="diskusibanding">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>

                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;">Apakah Anda mau melibatkan "orang lain" membantu Anda dalam Proses Banding?</td>
                                                <td style="border: 1px solid gray;" colspan="3" style="text-align:center; ">
                                                    <select class="input form-select" name="melibatkan" id="melibatkan">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>

                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;" colspan="9">Banding ini diajukan atas Keputusan Asesmen yang dibuat terhadap Skema Sertifikasi <br> (Kualifikasi/Klaster/Okupasi) berikut :</td>

                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Skema Sertifikasi &nbsp; &nbsp; &nbsp; :</td>
                                                <td style="border: 1px solid gray;" colspan="6"><input type="text" size="48" name="skemasertif" class="form-control" value="" id="skemasertif" disabled> </td>
                                            </tr>
                                            <tr>

                                            </tr>
                                            <td style="border: 1px solid gray;">No.Skema Sertifikasi :</td>
                                            <td style="border: 1px solid gray;" colspan="6"><input type="text" name="noskema" class="form-control" size="48" value="" id="noskema" disabled> </td>
                                            <tr>
                                                <td style="border: 1px solid gray;"><label for="bandin">Banding ini diajukan atas alasan sebagai berikut :</label></td>
                                                <td style="border: 1px solid gray;" colspan="6"><textarea cols="55" rows="4" style="font-weight: bold;" name="alasanbanding" id="alasanbanding"></textarea></td>

                                            </tr>

                                        </table>

                                        <p>Anda mempunyai hak mengajukan banding jika Anda menilai proses asesmen tidak sesuai SOP dan tidak memenuhi Prinsip Asesmen</p>


                                        <table border="1" class="table" style="text-align:center;">

                                            <tr style="border: 1px solid gray;">
                                                <th style="border: 1px solid gray; text-align:center;">Tanda Tangan Asesi</th>
                                                <th style="border: 1px solid gray; text-align:center;">Tanggal</th>
                                            </tr>

                                            <tr>
                                                <td style="border: 1px solid gray;">
                                                    <input type="file" value="Pilih File" id="upload-ttdasesi">
                                                </td>

                                                <td style="border: 1px solid gray;">
                                                    <input type="date" class="form-control" id="tanggal2" name="tanggal2" value="">
                                                </td>
                                            </tr>

                                        </table>

                                        <br>

                                        <button type="submit" value="Kirim" class="btn btn-primary">Kirim</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script src="{{ asset('assets/js/uji_kompetensi/ba04.js') }}"></script> -->


@endsection