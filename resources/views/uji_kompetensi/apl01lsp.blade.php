@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/apl01-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <!-- ppppppppppppppppppppppppppppp -->
                                    <div id="content" class="invoice-box" style="color: black;">


                                        <!-- <form enctype="multipart/form-data"> -->
                                        <h3>FR.APL.01. PERMOHONAN SERTIFIKASI KOMPETENSI</h3>

                                        <h4>Bagian 1 : Rincian Data Pemohon Sertifikasi</h4>
                                        <p>Pada bagian ini, cantumlah data pribadi, data pendidikan formal serta data pekerjaan anda pada saat ini</p>

                                        <h4>a. Data Pribadi</h4>
                                        <p class="wd text-danger">*WAJIB DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' ) </p>

                                        <table class="table1">
                                            <tr>
                                                <td><label for="nama">Nama lengkap</label></td>
                                                <td>:</td>
                                                <td><output id="nama_asesi"> <?= $user[0]->nama_lengkap ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="nik">No. KTP/NIK/Paspor</label></td>
                                                <td>:</td>
                                                <td><output id="nik"> <?= $user[0]->nik ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tempatlahir">Tempat Lahir</label></td>
                                                <td>:</td>
                                                <td><output id="tempatlahir"> <?= $user[0]->tempat_lahir ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tanggallahir">Tanggal Lahir</label></td>
                                                <td>:</td>
                                                <td><output id="tanggallahir"> <?= $user[0]->tanggal_lahir ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="jeniskelamin">Jenis Kelamin</label></td>
                                                <td>:</td>
                                                <td><output id="jeniskelamin"> <?= $user[0]->nama_jenis_kelamin ?></output> </td>
                                            <tr>
                                                <td><label for="kebangsaan">Kebangsaan</label></td>
                                                <td>:</td>
                                                <td><output id="kebangsaan"> <?= $user[0]->nama_kebangsaan ?></output> </td>
                                            <tr>
                                                <td><label for="alamat">Alamat</label></td>
                                                <td>:</td>
                                                <td><output id="alamat"> <?= $user[0]->alamat ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kodepos">Kode pos</label></td>
                                                <td>:</td>
                                                <td><output id="kodepos"> <?= $user[0]->kodepos ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="prvns">Provinsi</label></td>
                                                <td>:</td>
                                                <td><output id="provinsi"> <?= $user[0]->nama_provinsi ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kabupatenkota">Kabupaten / Kota</label></td>
                                                <td>:</td>
                                                <td><output id="kabupatenkota"> <?= $kab_kota_user[0]->nama_kab_kota ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tlp">No. Telepon / Hp</label></td>
                                                <td>:</td>
                                                <td><output id="telepon"> <?= $user[0]->no_hp ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="email">Email</label></td>
                                                <td>:</td>
                                                <td><output id="email"> <?= $user[0]->email ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kualifikasipendidikan">kualifikasi Pendidikan</label></td>
                                                <td>:</td>
                                                <td><output id="kualifikasipendidikan"> <?= $user[0]->nama_kualifikasi_pendidikan ?></output> </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label for="pekerjaan">Pekerjaan</label></td>
                                                <td>:</td>
                                                <td><output id="pekerjaan"> <?= $user[0]->nama_pekerjaan ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-danger">*Pilih belum/tidak bekerja apabila belum bekerja atau pelajar/mahasiswa apabila masih pelajar.</td>
                                            </tr>
                                        </table>
                                        <br>
                                        <h4>b. Data Pekerjaan Sekarang</h4>
                                        <p class="wd text-danger">*JIKA BELUM BEKERJA, TIDAK PERLU DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>
                                        <table class="table2">
                                            <tr>
                                                <td><label for="namainstitusiperusahaan">Nama Insitusi/Perusahaan</label></td>
                                                <td>:</td>
                                                <td><output id="namainstitusiperusahaan"><?= $user[0]->institusi_perusahaan ?></output> </td>
                                                <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="namainstitusiperusahaan" name="namainstitusiperusahaan" placeholder="Masukan nama Insitusi / Perusahaan anda"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="jabatan">Jabatan</label></td>
                                                <td>:</td>
                                                <td><output id="jabatan"><?= $user[0]->jabatan ?></output> </td>
                                                <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="jabatan" name="jabatan" placeholder="Masukan nama jabatan anda"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="alamatkantor">Alamat kantor</label></td>
                                                <td>:</td>
                                                <td><output id="alamatkantor"><?= $user[0]->alamat_kantor ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kodepos2">Kode pos</label></td>
                                                <td>:</td>
                                                <td><output id="kodepos2"><?= $user[0]->kodepos_kantor ?></output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="telepon2">No. Telepon Kantor </label></td>
                                                <td>:</td>
                                                <td><output id="telepon2"><?= $user[0]->no_hp_kantor ?></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="fax2">Fax</label></td>
                                                <td>:</td>
                                                <td><output id="fax2"><?= $user[0]->fax_kantor ?></output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="email2">Email Kantor</label></td>
                                                <td>:</td>
                                                <td><output id="email2"><?= $user[0]->email_kantor ?></output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-danger" style="opacity: 0;">*Pilih belum/tidak bekerja apabila belum bekerja atau pelajar/mahasiswa apabila masih pelajar.</td>
                                            </tr>
                                        </table>

                                        <h3>Bagian 2 : Data Sertifikasi</h3>
                                        <p>Tuliskan Judul dan Nomor Skema Sertifikasi yang anda ajukan berikut Daftar Unit Kompetensi sesuai kemasan pada skema sertifikasi untuk mendapatkan pengakuan sesuai dengan latar belakang pendidikan, pelatihan serta pengalaman kerja yang anda miliki.</p>

                                        <input type="hidden" name="idJadwalAsesor" id="idJadwalAsesor">

                                        <table class="table" style="border: 1px solid black;">
                                            <tr class="hide">
                                                <td>TUK</td>
                                                <td>:</td>
                                                <td id="el-tuk">
                                                    <input type="text" name="tuk" id="tuk" disabled />
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Nama Jadwal</td>
                                                <td style="border: 1px solid black;"><output id="juduljadwal"><?= $res_jadwal_asesmen->nama_jadwal ?></output></td>

                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" rowspan="2">Skema Sertifikasi (KKNI/Okupasi/Klaster)</td>
                                                <td style="border: 1px solid black;"><label for="jdl">Judul </label></td>
                                                <td style="border: 1px solid black;">
                                                    <!-- <div class="el-skema"> -->
                                                    <output id="skema"><?= $res_jadwal_asesmen->skema ?></output>
                                                </td>
                                                <!-- </div> -->

                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;"><label for="nmr">Nomor </label></td>
                                                <td style="border: 1px solid black;"><output id="nomor"><?= $res_jadwal_asesmen->nomor ?></output></td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Nama Asesor</td>
                                                <td style="border: 1px solid black;"><output id="namaasesor"><?= $res_jadwal_asesor->asesor ?></output></td>
                                            </tr>
                                            <tr class="hide">
                                                <td>No. Reg. Asesor</td>
                                                <td>:</td>
                                                <td id="el-no_Reg">
                                                    <input type="text" name="no_Reg" id="no_Reg" disabled value="<?= $res_jadwal_asesor->no_reg ?>" />
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2"><label>Tujuan Asesmen</label></td>
                                                <td style="border: 1px solid black;">
                                                    <select name="tujuanasesmen" id="tujuanasesmen" class="form-select required">
                                                        <option value="">Pilih Tujuan Asesmen</option>
                                                        <?php foreach ($list_tujuan_asesmen as $row) : ?>
                                                            <option value="<?= $row->id ?>"><?= $row->nama_tujuan_asesmen ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Tanggal Asesmen</td>
                                                <!-- <td> -->
                                                <td style="border: 1px solid black;"><output id="tanggalasesmen"><?= $res_jadwal_asesmen->tanggal_spk ?></output> </td>
                                                <!-- <div id="el-tanggalasesmen">
              <input type="text" name="tanggal" id="tanggalasesmen" disabled>
            </div> -->
                                                <!-- </td> -->
                                            </tr>
                                        </table>

                                        <br>
                                        <form id="formsbs">
                                            <p><strong>Daftar Unit Kompetensi sesuai kemasan:</strong></p>
                                            <table class="table" style="border: 1px solid black;" border-collapse="collapse">
                                                <tr style="border: 1px solid black;" class="align-center">
                                                    <th style="background:#fac090;">No.</th>
                                                    <th style="background:#fac090;">Kode Unit</th>
                                                    <th style="background:#fac090;">Judul Unit</th>
                                                </tr>
                                                <?php $i = 1;
                                                foreach ($get_skema as $row) : ?>
                                                    <tr style="border: 1px solid black;" class="table6">
                                                        <td style="border: 1px solid black;" class="align-center">
                                                            <label for="noUrut"><?= $i ?></label>
                                                        </td>
                                                        <td style="border: 1px solid black;" class="align-center">
                                                            <output id="no_uk" name="no_uk<?= $i ?>" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                        </td>
                                                        <td style="border: 1px solid black;">
                                                            <output value="<?= $row->nama ?>" id="uk" name="uk<?= $i ?>" class=" bg2"><?= $row->nama ?></output>
                                                        </td>
                                                    </tr>
                                                <?php $i++;
                                                endforeach; ?>
                                            </table>
                                        </form>
                                        <br>

                                        <h3>Bagian 3 : Bukti Kelengkapan Pemohon</h3>
                                        <p>Bukti Persyaratan Dasar Pemohon</p>
                                        <p class="wd">*FORMAT FILE HARUS BERBENTUK PDF ATAU GAMBAR <br> NAMA FILE TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>

                                        <table class="table">
                                            <tr>
                                                <th style="background:#fac090;" rowspan="2">No.</th>
                                                <th style="background:#fac090;" rowspan="2">Bukti Persyaratan Dasar</th>
                                                <th style="background:#fac090;" colspanrequired>Ada</th>
                                                <th style="background:#fac090;" rowspan="2">Tidak ada</th>
                                            </tr>
                                            <tr>
                                                <th style="background:#fac090;"><label>Memenuhi Syarat / Tidak Memenuhi Syarat</label></th>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">1.</td>
                                                <td style="border: 1px solid black;">Pas Foto 3x4 :
                                                    <div id="el-pasFoto">
                                                        <!-- <input type="text" class="form-input" id="pasFoto" name="pasFoto" value="" disabled> -->
                                                        <img id="pasFoto" name="pasFoto" style="width: 280px">
                                                    </div>

                                                    <input type="file" class="form-control" class="form-control" value="Pilih File" id="upload-pasFoto">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="pasFoto_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">2.</td>
                                                <td style="border: 1px solid black;">KTP :
                                                    <div id="el-ktp">
                                                        <img id="ktp" name="ktp" style="width: 280px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-ktp">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="ktp_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">3.</td>
                                                <td style="border: 1px solid black;">Ijazah :
                                                    <div id="el-ijazah">
                                                        <img id="ijazah" name="ijazah" style="width: 280px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-ijazah">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="ijazah_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">4.</td>
                                                <td style="border: 1px solid black;">Sertifikasi Pelatihan :
                                                    <div id="el-sertifikasipelatihan">
                                                        <img id="sertifikasipelatihan" name="sertifikasipelatihan" style="width: 280px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-sertifikasipelatihan">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled id="sertifikasipelatihan_ada" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">5.</td>
                                                <td style="border: 1px solid black;">CV :
                                                    <div id="el-cv">
                                                        <img id="cv" name="cv" style="width: 280px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-cv">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="cv_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">6.</td>
                                                <td style="border: 1px solid black;">Portofolio :
                                                    <div id="el-portofolio">
                                                        <img id="portofolio" name="portofolio" style="width: 280px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-portofolio">
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="portofolio_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                        </table>

                                        <br>

                                        <table class="table">
                                            <tr>
                                                <td style="border: 1px solid black;" rowspan="3" class="rekomendasi">Rekomendasi (diisi oleh LSP): <br> Berdasarkan ketentuan persyaratan dasar, maka pemohon : <br> (
                                                    <select classrequired namerequired id="rekomendasi" disabled>
                                                        <option value=""></option>
                                                        <option value="Diterima">Diterima</option>
                                                        <option value="Tidak Diterima">Tidak Diterima</option>
                                                    </select>
                                                    ) sebagai peserta sertifikasi
                                                </td>
                                                <td style="border: 1px solid black;" colspan="2">Pemohon/Kandidat:</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="namapk">Nama</label></td>
                                                <td style="border: 1px solid black;">
                                                    <p><output onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="required input" id="namapemohon" contenteditable="true"><?= $user[0]->nama_lengkap ?></output></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="ttdpk">Tanda tangan/</label><label for="tanggalpk">Tanggal</label></td>
                                                <td style="border: 1px solid black;">
                                                    <div id="el-ttdasesi">
                                                        <img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user[0]->tanda_tangan ?>" style="width: 280px">
                                                        <input type="hidden" id="ttdasesi" name="ttdasesi" value="<?= $user[0]->tanda_tangan ?>" disabled>
                                                    </div>

                                                    <!-- <input type = "submit" value = "Pilih File" id="upload-ttd"> -->
                                                    <br>
                                                    <div id="el-tanggalpemohon">
                                                        <input type="text" name="tanggal" id="tanggalpemohon" class="input form-control" value="<?= date('Y-m-d') ?>" disabled>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" rowspan="4">Catatan : <br>
                                                    <textarea disabled id="catatanadmin" class="catmin form-control"></textarea>
                                                </td>
                                                <td style="border: 1px solid black;" colspan="2">Admin LSP :</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="namaadmn">Nama</label></td>
                                                <td style="border: 1px solid black;" id="el-namaadmin"><input name="namaadmn" id="namaadmin" class="form-control" type="text" placeholder="Masukan nama admin"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="noreg">No. Pegawai</label></td>
                                                <td style="border: 1px solid black;"><input type="text" id="noreg" class="form-control" placeholder="Masukan nomor pegawai admin"></output></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="ttdpkadmn">Tanda tangan/</label><label for="tanggalpkadmn">Tanggal</label></td>
                                                <td style="border: 1px solid black;">
                                                    <div id="el-ttdadmin">
                                                        <img id="ttdadmin" name="ttdadmin" src="" style="width: 350px">
                                                    </div>
                                                    <input type="file" class="form-control" value="Pilih File" id="upload-ttd-admin">
                                                    <div id="el-tanggaladmin">
                                                        <input type="text" class="form-control" name="tanggal" id="tanggaladmin" value="<?= date('Y-m-d') ?>" disabled>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                        <br>

                                        <!-- </form> -->

                                        <input class="hide" type="submit" value="Kirim" onclick="k[x].app.kirim()" id="kirim">
                                        <input type="button" class="btn btn-primary" value="Verfikasi Form" onclick="kirim2();" id="kirim2">
                                        <input type="button" value="Kembali" id="lanjutapl02" class="apl02 hide btn-btn-secondary">

                                    </div>
                                    <!-- ppppppppppppppppppppppppppppp -->

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/uji_kompetensi/jadwal_lanjut.js') }}"></script> -->


@endsection