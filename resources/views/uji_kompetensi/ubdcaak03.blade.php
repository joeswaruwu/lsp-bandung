@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/ak03-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h3>FR.AK.03 UMPAN BALIK DAN CATATAN ASESMEN</h3>

                                        <!-- <form name="form1"> -->
                                        <table class="table">
                                            <tr>
                                                <td><label for="nama">Nama Asesi</label></td>
                                                <td>:</td>
                                                <td><output class="bg" type="text" name="namaasesi" id="namaasesi" placeholder="Masukkan nama anda" disabled></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="namasor">Nama Asesor</label></td>
                                                <td>:</td>
                                                <td><output class="bg" type="text" name="namaasesor" id="namaasesor" disabled></output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tanggal">Tanggal</label></td>
                                                <td>:</td>
                                                <td><output type="date" name="tanggal" id="tanggal"></output> </td>
                                            </tr>
                                        </table>

                                        <p>Umpan balik dari Asesi (diisi oleh Asesi setelah mengambil keputusan) :</p>

                                        <table class="table">
                                            <tr class="tab">
                                                <td style="border: 1px solid gray; text-align:center;" rowspan="2">KOMPONEN</td>
                                                <td style="border: 1px solid gray; text-align:center;">Hasil</td>
                                                <td style="border: 1px solid gray; text-align:center;" class="ab ctt" rowspan="2">Catatan / komentar asesi</td>
                                            </tr>
                                            <tr class="tab">
                                                <td style="border: 1px solid gray; text-align:center;" class="ab yata">Ya/Tidak</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi</td>
                                                <td style="border: 1px solid gray;"><select name="yata1" id="soal1" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket1"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Saya diberikan kesempatan untuk mempelajari standar kompetensi yang akan diujikan dan menilai diri sendiri terhadap pencapaiannya</td>
                                                <td style="border: 1px solid gray;"><select name="yata2" id="soal2" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket2"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Asesor memberikan kesempatan untuk mendiskusikan/menegosiasikan metoda, instrumen dan sumber asesmen serta jadwal asesmen </td>
                                                <td style="border: 1px solid gray;"><select name="yata3" id="soal3" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket3"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Asesor berusaha menggali seluruh bukti pendukung yang sesuai dengan latar belakang pelatihan dan pengalaman yang saya miliki</td>
                                                <td style="border: 1px solid gray;"><select name="yata4" id="soal4" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket4"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Saya sepenuhnya diberikan kesempatan untuk mendemonstrasikan kompetensi yang saya miliki selama asesmen</td>
                                                <td style="border: 1px solid gray;"><select name="yata5" id="soal5" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket5"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Saya mendapatkan penjelasan yang memadai mengenai keputusan asesmen</td>
                                                <td style="border: 1px solid gray;"><select name="yata6" id="soal6" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket6"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Asesor memberikan umpan balik yang mendukung setelah asesmen serta tindak lanjutnya</td>
                                                <td style="border: 1px solid gray;"><select name="yata7" id="soal7" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket7"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Asesor bersama saya mempelajari semua dokumen asesmen serta menandatanganinya</td>
                                                <td style="border: 1px solid gray;"><select name="yata8" id="soal8" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket8"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Saya mendapatkan jaminan kerahasiaan hasil asesmen serta penjelasan penanganan dokumen asesmen</td>
                                                <td style="border: 1px solid gray;"><select name="yata9" id="soal9" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket9"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;">Asesor menggunakan keterampilan komunikasi yang efektif selama asesmen</td>
                                                <td style="border: 1px solid gray;"><select name="yata10" id="soal10" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                </td>
                                                <td style="border: 1px solid gray;"><textarea class="form-control" id="ket10"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid gray;" class="ab kmntr">Catatan/komentar lainnya (apabila ada) :</td>
                                                <td style="border: 1px solid gray;" colspan="3"><textarea class="form-control" id="keterangan"></textarea></td>
                                            </tr>
                                        </table>
                                        <!-- </form> -->
                                        <br>
                                        <br>
                                        <button type="submit" value="Kirim" class="btn btn-primary">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script src="{{ asset('assets/js/uji_kompetensi/ba04.js') }}"></script> -->


@endsection