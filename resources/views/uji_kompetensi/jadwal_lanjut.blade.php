@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <label for=""> <strong>*Klik asesor yang ditunjuk untuk menuju ke APL01</strong> </label>
                                        <br>
                                        <br>
                                        <table id="jadwalLanjut" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Asesor</th>
                                                    <th>No Reg</th>
                                                    <th>Jumlah Asesi</th>
                                                    <th>Nama LSP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- onclick=\"window.location='".$links[$i]."'\" -->
                                                <?php foreach ($jadwal_asesor as $row) : ?>
                                                    <tr>
                                                        <td>
                                                            <div style="margin-left:15%">
                                                                <a href="/apl01lsp/<?= $row->id ?>/<?= $id_jadwal_asesmen ?>" class="btn btn-outline-primary btn-sm block">Pilih</a>
                                                            </div>
                                                        </td>
                                                        <td><?= $row->nama_jadwal ?></td>
                                                        <td><?= $row->tanggal_uji ?></td>
                                                        <td><?= $row->asesor ?></td>
                                                        <td><?= $row->no_reg ?></td>
                                                        <td><?= $row->jumlah_asesi ?></td>
                                                        <td><?= $row->nama_lsp ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<script src="{{ asset('assets/js/uji_kompetensi/jadwal_lanjut.js') }}"></script>


@endsection