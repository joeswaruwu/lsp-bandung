@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <label for=""> <strong> *Klik Jadwal Asesmen yang ingin dilaksanakan untuk memilih asesor yang ditunjuk</strong></label>
                                        <br>
                                        <br>
                                        <table id="tblPskapl01" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>TUK</th>
                                                    <th>Tema</th>
                                                    <th>Skema</th>
                                                    <th>Nomor</th>
                                                    <th>Id Skema</th>
                                                    <th>Target Peserta</th>
                                                    <th>Jam Pengujian</th>
                                                    <th>Lokasi Pengujian</th>
                                                    <th>Sumber Anggaran</th>
                                                    <th>Anggaran Per Asesi</th>
                                                    <th>Nomor SPK</th>
                                                    <th>Tanggal SPK</th>
                                                    <th>Tanggal Mulai Pelatihan</th>
                                                    <th>Tanggal Akhir Pelatihan</th>
                                                    <th>Durasi Pelatihan</th>
                                                    <th>Nama Pelatihan</th>
                                                    <th>Nama Instansi</th>
                                                    <th>Nama LSP</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<script src="{{ asset('assets/js/uji_kompetensi/pskapl01.js') }}"></script>


@endsection