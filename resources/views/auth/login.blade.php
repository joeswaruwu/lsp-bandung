@extends('template_auth.main')
@section('isiAuth')
<div id="auth">
    <div class="row col-lg-12">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-5 mt-5">
            <div id="auth-left">
                <p style="font-size:40px; color:black " class="auth-title">Log in</p>
                <p class="auth-subtitle mb-5" style="color:black ">Welcome To LSP.</p>

                <form action="/validasiLogin" method="POST" class="pt-3">
                    @csrf
                    <div class="form-group position-relative has-icon-left mb-4">
                        <input type="text" id="email" name="email" class="form-control form-control" placeholder="Email">
                        <div class="form-control-icon">
                            <i class="bi bi-person"></i>
                        </div>
                    </div>
                    <div class="form-group position-relative has-icon-left mb-4">
                        <input id="password" name="password" type="password" class="form-control form-control" placeholder="Password">
                        <div class="form-control-icon">
                            <i class="bi bi-shield-lock"></i>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-lg shadow-lg mt-3" style="background-color:#009999; color:white; font-size: 15px;"> Masuk</button>
                </form>
                <div class="text-center mt-5 text-lg fs-4">
                    <p style="color: black; font-size:15px">Belum punya akun? <a href="auth-register.html" class="font-bold">Daftar</a>.</p>
                    <p style="color: black; font-size:15px"><a href="auth-register.html" class="font-bold">Lupa Password</a>.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3">

        </div>
    </div>
</div>
@endsection