@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default" onclick="cancel()">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblDaftarAdmin" class="table ">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Jenis LSP</th>
                                                    <th>Nama</th>
                                                    <th>No Telp</th>
                                                    <th>No Telp Kantor</th>
                                                    <th>Fax Kantor</th>
                                                    <th>Email</th>
                                                    <th>Password</th>
                                                    <th>Website</th>
                                                    <th>Alamat Kantor</th>
                                                    <th>Kode Pos</th>
                                                    <th>No SK LSP</th>
                                                    <th>Dokumen SK LSP</th>
                                                    <th>No Lisensi LSP</th>
                                                    <th>Dokumen Lisensi LSP</th>
                                                    <th>Npwp</th>
                                                    <th>Dokumen Npwp</th>
                                                    <th>Rekening Bank</th>
                                                    <th>Dokumen Rekening Bank</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1">Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDaftarAdmin" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <label for=""> <strong style="color:black; "> 1. Data LSP </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="jenis_lsp">Jenis LSP</label>
                                        <fieldset class="form-group">
                                            <select id="jenis_lsp" name="jenis_lsp" class="form-select">
                                                <option value="">-- Pilih Jenis LSP --</option>
                                                <?php foreach ($list_jenis_lsp as $row) { ?>
                                                    <option value="<?= $row->id ?>"><?= $row->nama_jenis_lsp ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama">Nama</label>
                                        <input style="color:black" type="text" id="nama" class="form-control" placeholder="Masukkan Nama" name="nama" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_hp">No. Hp</label>
                                        <input style="color:black" type="number" id="no_hp" class="form-control" placeholder="Masukkan No. HP / WhatsApp" name="no_hp" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_hp_kantor">No. Telepon Kantor</label>
                                        <input style="color:black" type="number" id="no_hp_kantor" class="form-control" placeholder="Masukkan No. Telepon Kantor" name="no_hp_kantor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_fax">No. Fax</label>
                                        <input style="color:black" type="number" id="no_fax" class="form-control" placeholder="Masukkan No. Fax Kantor" name="no_fax" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="email">Email</label>
                                        <input style="color:black" type="text" id="email" class="form-control" name="email" placeholder="Masukkan email" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="password">Password</label>
                                        <input style="color:black" type="text" id="password" class="form-control" name="password" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="website">Website</label>
                                        <input style="color:black" type="text" id="website" class="form-control" name="website" placeholder="Masukkan website LSP" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kode_pos">Kode Pos</label>
                                        <input style="color:black" type="number" id="kode_pos" class="form-control" name="kode_pos" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="alamat_kantor">Alamat Kantor</label>
                                        <textarea style="color:black" class="form-control" id="alamat_kantor" name="alamat_kantor" rows="3"></textarea>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                            <label for=""> <strong style="color:black; "> 2. Data Dokumen LSP </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_sk_lsp">No. SK</label>
                                        <input style="color:black" type="text" id="no_sk_lsp" class="form-control" placeholder="Masukkan nomor SK LSP" name="no_sk_lsp" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="dokumen_sk_lsp">Dokumen SK</label>
                                        <br>
                                        <embed id="riview_dokumen_sk_lsp" name="riview_dokumen_sk_lsp" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="dokumen_sk_lsp" name="dokumen_sk_lsp">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_lisensi_lsp">No. Lisensi</label>
                                        <input style="color:black" type="text" id="no_lisensi_lsp" class="form-control" placeholder="Masukkan no. lisensi LSP" name="no_lisensi_lsp" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="dokumen_lisensi_lsp">Dokumen Lisensi</label>
                                        <br>
                                        <embed id="riview_dokumen_lisensi_lsp" name="riview_dokumen_lisensi_lsp" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="dokumen_lisensi_lsp" name="dokumen_lisensi_lsp">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="npwp">No. NPWP</label>
                                        <input style="color:black" type="text" id="npwp" class="form-control" placeholder="Masukkan no. NPWP LSP" name="npwp" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="dokumen_npwp">Dokumen NPWP</label>
                                        <br>
                                        <embed id="riview_dokumen_npwp" name="riview_dokumen_npwp" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="dokumen_npwp" name="dokumen_npwp">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="rekening_bank">No. Rekening Bank</label>
                                        <input style="color:black" type="number" id="rekening_bank" class="form-control" placeholder="Masukkan no. rekening bank LSP" name="rekening_bank" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="dokumen_rek_bank">Dokumen Rekening Bank</label>
                                        <br>
                                        <embed id="riview_dokumen_rek_bank" name="riview_dokumen_rek_bank" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="dokumen_rek_bank" name="dokumen_rek_bank">
                                    </div>
                                </div>

                            </div>

                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="{{ asset('assets/js/master_data/daftar_admin.js') }}"></script>


@endsection