@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblDaftarPenyelia" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Email</th>
                                                    <th>Password</th>
                                                    <th>Nama</th>
                                                    <th>Akses</th>
                                                    <th>Alamat</th>
                                                    <th>Kode Pos</th>
                                                    <th>Provinsi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDaftarPenyelia" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" id="id" name="id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="email">Email</label>
                                        <input style="color:black" type="email" class="form-control" id="email" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="password">Password</label>
                                        <input style="color:black" type="password" class="form-control" id="password" name="password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama">Nama</label>
                                        <input style="color:black" type="text" id="nama" class="form-control" name="nama" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_hp">Akses</label>
                                        <select id="akses" name="akses" class="form-select">
                                            <option value="">-- Pilih Akses --</option>
                                            <?php foreach ($list_akses as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->nama_role ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="alamat">Alamat</label>
                                        <textarea style="color:black" class="form-control" id="alamat" name="alamat" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kode_pos">Kode Pos</label>
                                        <input style="color:black" type="number" id="kode_pos" class="form-control" placeholder="Masukkan Kode Pos" name="kode_pos" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="provinsi">Provinsi</label>
                                        <fieldset class="form-group">
                                            <select id="provinsi" name="provinsi" class="form-select" id="provinsi">
                                                <option value="">-- Pilih Provinsi --</option>
                                                <?php foreach ($provinsi as $row) { ?>
                                                    <option value="<?= $row->kode ?>"><?= $row->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kab_kota">Kabupaten / Kota</label>
                                        <fieldset class="form-group">
                                            <select id="kab_kota" name="kab_kota" class="form-select" id="kab_kota">
                                                <option value="">-- Pilih Provinsi Terlebih dahulu --</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>


                            </div>


                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="{{ asset('assets/js/master_data/daftar_penyelia.js') }}"></script>


@endsection