@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblStrukturJabatanLsp" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Jabatan</th>
                                                    <th>Nama</th>
                                                    <th>Nip</th>
                                                    <th>Tanda Tangan</th>
                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveStrukturJabatanLsp" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" id="id" name="id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="no_hp">Jabatan</label>
                                        <select id="jabatan" name="jabatan" class="form-select">
                                            <option value="">-- Pilih Jabatan --</option>
                                            <?php foreach ($list_jabatan as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->nama_jabatan ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama">Nama</label>
                                        <input style="color:black" type="text" id="nama" class="form-control" name="nama" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nip">Nip</label>
                                        <input style="color:black" type="number" id="nip" class="form-control" placeholder="Masukkan Nip" name="nip" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="ttd">Tanda Tangan</label>
                                        <img name="riview_ttd" id="riview_ttd" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="ttd" name="ttd">
                                    </div>
                                </div>

                            </div>


                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('assets/js/master_data/struktur_jabatan_lsp.js') }}"></script>


@endsection