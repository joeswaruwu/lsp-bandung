@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <?= $title_sub_menu; ?>
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" action="/saveProfileAsesi" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        <label for=""> <strong style="color:black; "> 1. Data Akun </strong></label>
                                        <div class="row mt-3">

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="email">Email</label>
                                                    <input style="color:black" type="text" id="email" class="form-control" placeholder="Masukkan Email" name="email" value="<?= $user->email ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="password">Password</label>
                                                    <input style="color:black" type="text" id="password" class="form-control" placeholder="Masukkan Password" name="password" value="<?= $user->password ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <label for=""> <strong style="color:black; "> 2. Data Pribadi </strong></label>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="name">Nama Lengkap</label>
                                                    <input style="color:black" type="text" id="name" class="form-control" placeholder="Masukkan Nama Lengkap" name="name" value="<?= $user->name ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="nik">No. KTP/NIK/Paspor</label>
                                                    <input style="color:black" type="number" id="nik" class="form-control" placeholder="Masukkan Nik" name="nik" value="<?= $user->nik ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tempat_lahir">Tempat Lahir</label>
                                                    <input style="color:black" type="text" id="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" name="tempat_lahir" value="<?= $user->tempat_lahir ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tanggal_lahir">Tanggal Lahir</label>
                                                    <input style="color:black" type="date" id="tanggal_lahir" class="form-control" placeholder="Masukkan Tanggal Lahir" name="tanggal_lahir" value="<?= $user->tanggal_lahir ?>" />
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_jenis_kelamin -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="jenis_kelamin">Jenis Kelamin</label>
                                                    <fieldset class="form-group">
                                                        <select id="jenis_kelamin" name="jenis_kelamin" class="form-select" id="jenis_kelamin">
                                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                                            <?php foreach ($jenis_kelamin as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->jenis_kelamin == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_keabangsaan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kebangsaan">Kebangsaan</label>
                                                    <fieldset class="form-group">
                                                        <select id="kebangsaan" name="kebangsaan" class="form-select" id="kebangsaan">
                                                            <option value="">-- Pilih Kebangsaan --</option>
                                                            <?php foreach ($kebangsaan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->kebangsaan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="alamat">Alamat</label>
                                                    <textarea style="color:black" class="form-control" id="alamat" name="alamat" rows="3"><?= $user->alamat ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kode_pos">Kode Pos</label>
                                                    <input style="color:black" type="number" id="kode_pos" class="form-control" name="kode_pos" value="<?= $user->kodepos ?>" />
                                                </div>
                                            </div>
                                            <!-- terapin konsep seelct pemilihan alamat -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="provinsi">Provinsi</label>
                                                    <fieldset class="form-group">
                                                        <select id="provinsi" name="provinsi" class="form-select" id="provinsi">
                                                            <option value="">-- Pilih Provinsi --</option>
                                                            <?php foreach ($provinsi as $row) { ?>
                                                                <option value="<?= $row->kode ?>" <?= $user->provinsi == $row->kode ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kab_kota">Kabupaten / Kota</label>
                                                    <fieldset class="form-group">
                                                        <select id="kab_kota" name="kab_kota" class="form-select" id="kab_kota">
                                                            <?php if (!$kabupaten_kota) { ?>
                                                                <option value="">-- Pilih Provinsi Terlebih dahulu --</option>
                                                                <?php } else {
                                                                foreach ($kabupaten_kota as $row) { ?>
                                                                    <option value="<?= $row->kode ?>"><?= $row->nama ?></option>
                                                            <?php }
                                                            } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_hp">No. HP / WhatsApp</label>
                                                    <input style="color:black" type="number" id="no_hp" class="form-control" placeholder="Masukkan No. Telepon" name="no_hp" value="<?= $user->no_hp ?>" />
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_pendidikan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="pendidikan">Kualifikasi Pendidikan</label>
                                                    <fieldset class="form-group">
                                                        <select id="pendidikan" name="pendidikan" class="form-select" id="pendidikan">
                                                            <option value="">-- Pilih Pendidikan --</option>
                                                            <?php foreach ($pendidikan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->kualifikasi_pendidikan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_pekerjaan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="pekerjaan">Pekerjaan*</label>
                                                    <fieldset class="form-group">
                                                        <select id="pekerjaan" name="pekerjaan" class="form-select" id="pekerjaan" required>
                                                            <option value="">-- Pilih Pekerjaan --</option>
                                                            <?php foreach ($pekerjaan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->pekerjaan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tanda_tangan">Tanda Tangan Asesi</label>
                                                    <br>
                                                    <?php if ($user->tanda_tangan != "") { ?>
                                                        <embed src="assets/document/tanda_tangan/asesi/<?= $user->tanda_tangan  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="tanda_tangan" name="tanda_tangan">
                                                </div>
                                            </div>
                                            <small class="text-danger">*Jika belum bekerja, pilih Belum/Tidak Bekerja ATAU Pelajar/Mahasiswa</small>
                                        </div>
                                        <br>
                                        <br>
                                        <label for=""> <strong style="color:black; "> 3. Data Pekerjaan**</strong></label>
                                        <br>
                                        <small class="text-danger">**JIKA BELUM BEKERJA, TIDAK PERLU DIISI</small>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="institusi_perusahaan">Nama Institusi / Perusahaan</label>
                                                    <input style="color:black" type="text" id="institusi_perusahaan" class="form-control" placeholder="Masukkan Nama Institusi Perusahaan" name="institusi_perusahaan" value="<?= $user->institusi_perusahaan ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="jabatan">Jabatan</label>
                                                    <input style="color:black" type="text" id="jabatan" class="form-control" placeholder="Masukkan Nama Jabatan" name="jabatan" value="<?= $user->jabatan ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="alamat_kantor">Alamat Kantor</label>
                                                    <textarea style="color:black" class="form-control" id="alamat_kantor" name="alamat_kantor" rows="3"><?= $user->alamat_kantor ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kode_pos_kantor">Kode Pos Kantor</label>
                                                    <input style="color:black" type="number" id="kode_pos_kantor" class="form-control" placeholder="Masukkan Kode Pos Kantor" name="kode_pos_kantor" value="<?= $user->kodepos_kantor ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_hp_kantor">No. Telp Kantor</label>
                                                    <input style="color:black" type="number" id="no_hp_kantor" class="form-control" placeholder="Masukkan No. Telp Kantor" name="no_hp_kantor" value="<?= $user->no_hp_kantor ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="fax_kantor">Fax Kantor</label>
                                                    <input style="color:black" type="number" id="fax_kantor" class="form-control" placeholder="Masukkan Nomor Fax Kantor" name="fax_kantor" value="<?= $user->fax_kantor ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="email_kantor">Email Kantor</label>
                                                    <input style="color:black" type="email" id="email_kantor" class="form-control" placeholder="Masukkan Email Kantor" name="email_kantor" value="<?= $user->email_kantor ?>" />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-12 d-flex justify-content-end mt-4">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">
                                                Simpan
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>

<script>
    $("#provinsi").change(function(event) {
        let valProvinsi = document.getElementById("provinsi").value;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "getKabKota",
            type: "post",
            data: {
                valProvinsi: valProvinsi,
                panjang: 5
            },
            dataType: "json",
            success: function(data) {
                let valKabKota = "";
                data.forEach(element => {
                    valKabKota += `
                    <option value="${element.kode}">${element.nama}</option>
                    `
                });
                $("#kab_kota").html(valKabKota);
            },
        });
    });
</script>
@endsection