@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4>Latest Comments</h4> -->
                            </div>
                            <div class="card-body" style=" margin: auto;  text-align: center;">
                                <h2>Selamat Datang di</h2>
                                <h2>Sistem LSP</h2>
                                <br>
                                <img src="assets/logo/logo_lsp_bandung.jpg" alt="" width="20%">
                                <br>
                                <br>
                                <br>

                                <label class="text-black">Developed By</label>
                                <br>
                                <label class="text-black"> PT. QIA Solution</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
@endsection