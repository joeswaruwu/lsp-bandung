@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card-body">
                            <form id="form_crud" class="form" action="/saveJadwalAsesor" enctype="multipart/form-data" method="POST">
                                @csrf
                                <input type="hidden" name="id" id="id">
                                <label for=""> <strong style="color:red; ">Lakukan penambahan jadwal asesmen terlebih dahulu sebelum Anda membuat jadwal asesor. </strong></label>
                                <div class="row mt-3">

                                    <div class="col-md-12 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="nama_jadwal">Nama Jadwal</label>
                                            <select id="nama_jadwal" name="nama_jadwal" class="form-select">
                                                <option value="">Pilih Nama Jadwal</option>
                                                <?php foreach ($nama_jadwal as $row) { ?>
                                                    <option value="<?= $row->id ?>"><?= $row->nama_jadwal ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="tanggal_uji">Tanggal Uji</label>
                                            <input style="color:black" type="text" id="tanggal_uji" class="form-control" name="tanggal_uji" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="asesor">Asesor yang ditugaskan</label>
                                            <select id="asesor" name="asesor" class="form-select">
                                                <option value="">Pilih Nama Asesor</option>
                                                <?php foreach ($asesor as $row) { ?>
                                                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="no_reg">No. Reg.</label>
                                            <input style="color:black" type="text" id="no_reg" class="form-control" name="no_reg" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="jumlah_asesi">Jumlah Asesi</label>
                                            <input style="color:black" type="number" id="jumlah_asesi" class="form-control" name="jumlah_asesi" />
                                        </div>
                                    </div>


                                </div>


                                <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                    <button type="submit" class="btn btn-primary me-1 mb-1">
                                        Simpan
                                    </button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>




<script src="{{ asset('assets/js/manajemen_jadwal/penugasan_asesor.js') }}"></script>


@endsection