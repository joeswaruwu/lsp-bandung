@extends('template_dashboard.main')
@section('isiDashboard')
<style>
    .table1 tr,
    .table2 tr,
    .table3 tr,
    .table4 tr {
        text-align: justify;
    }

    .table4 {
        width: 40% !important;
        margin-left: 40px;
    }

    .table5 {
        width: 40% !important;
    }

    .table5 tr {
        text-align: center;
    }
</style>

<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Surat Tugas Baru
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblSuratTugas" class="table1">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal_Uji</th>
                                                    <th>Nama_Jadwal</th>
                                                    <th>TUK</th>
                                                    <th>Skema</th>
                                                    <th>Sumber_Anggaran</th>
                                                    <th>Nomor_SPK</th>
                                                    <th>Tanggal_SPK</th>
                                                    <th>Tanggal_Surat</th>
                                                    <th>Lokasi</th>
                                                    <th>Nomor_Keputusan</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>

<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveSuratTugas" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <label for=""> <strong style="color:black; "> 1. Data Jadwal Asesmen </strong></label>
                            <div class="row mt-3">

                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_jadwal">Nama Jadwal</label>
                                        <select id="nama_jadwal" name="nama_jadwal" class="form-select" id="basicSelect" required>
                                            <option value="">Pilih Nama Jadwal</option>
                                            <?php foreach ($nama_jadwal as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->nama_jadwal ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="skema">Skema</label>
                                        <input style="color:black" type="text" id="skema" class="form-control" name="skema" readonly required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_spk">Nomor SPK</label>
                                        <input style="color:black" type="text" id="nomor_spk" class="form-control" name="nomor_spk" readonly required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_spk">tanggal SPK</label>
                                        <input style="color:black" type="text" id="tanggal_spk" class="form-control" name="tanggal_spk" readonly required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="sumber_dana">Sumber Dana</label>
                                        <input style="color:black" type="text" id="sumber_dana" class="form-control" name="sumber_dana" readonly required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_tuk">Nama TUK</label>
                                        <input style="color:black" type="text" id="nama_tuk" class="form-control" name="nama_tuk" readonly required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_uji">Tanggal Uji</label>
                                        <input style="color:black" type="date" id="tanggal_uji" class="form-control" name="tanggal_uji" required />
                                    </div>
                                </div>


                            </div>
                            <br>
                            <br>
                            <label for=""> <strong style="color:black; "> 2. Detail Surat Tugas </strong></label>
                            <div class="row mt-3">

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="lokasi">Surat dibuat di</label>
                                        <input style="color:black" type="text" id="lokasi" class="form-control" name="lokasi" placeholder="Masukkan Nama Kota" required />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_surat">Tanggal Surat</label>
                                        <input style="color:black" type="date" id="tanggal_surat" class="form-control" name="tanggal_surat" required />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_surat">Nomor Surat Tugas Asesor</label>
                                        <input style="color:black" type="text" id="nomor_surat" class="form-control" name="nomor_surat" required />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_keputusan">Nomor Surat Keputusan
                                            Direktur Eksekutif LSP</label>
                                        <input style="color:black" type="text" id="nomor_keputusan" class="form-control" name="nomor_keputusan" required />
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/manajemen_jadwal/surat_tugas.js') }}"></script>


@endsection