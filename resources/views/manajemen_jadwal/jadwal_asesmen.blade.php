@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Jadwal Baru
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblJadwalAsesmen" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>TUK</th>
                                                    <th>Tema</th>
                                                    <th>Skema</th>
                                                    <th>Nomor</th>
                                                    <th>Id Skema</th>
                                                    <th>Target Peserta</th>
                                                    <th>Jam Pengujian</th>
                                                    <th>Lokasi Pengujian</th>
                                                    <th>Sumber Anggaran</th>
                                                    <th>Anggaran Per Asesi</th>
                                                    <th>Nomor SPK</th>
                                                    <th>Tanggal SPK</th>
                                                    <th>Tanggal Mulai Pelatihan</th>
                                                    <th>Tanggal Akhir Pelatihan</th>
                                                    <th>Durasi Pelatihan</th>
                                                    <th>Nama Pelatihan</th>
                                                    <th>Nama Instansi</th>
                                                    <th>Nama LSP</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveJadwalAsesmen" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <label for=""> <strong style="color:black; "> 1. Data Pelatihan </strong></label>
                            <div class="row mt-3">

                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_pelatihan">Nama Pelatihan</label>
                                        <input style="color:black" type="text" id="nama_pelatihan" class="form-control" placeholder="Masukkan Nama Pelatihan" name="nama_pelatihan" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_mulai_pelatihan">Tanggal Mulai Pelatihan</label>
                                        <input style="color:black" type="date" id="tanggal_mulai_pelatihan" class="form-control" name="tanggal_mulai_pelatihan" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_akhir_pelatihan">Tanggal Akhir Pelatihan</label>
                                        <input style="color:black" type="date" id="tanggal_akhir_pelatihan" class="form-control" name="tanggal_akhir_pelatihan" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_instansi">Nama Instansi</label>
                                        <input style="color:black" type="text" id="nama_instansi" class="form-control" name="nama_instansi" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="durasi_pelatihan">Durasi Pelatihan</label>
                                        <input style="color:black" type="number" id="durasi_pelatihan" class="form-control" name="durasi_pelatihan" />
                                    </div>
                                </div>


                            </div>
                            <br>
                            <br>
                            <label for=""> <strong style="color:black; "> 2. Data Jadwal Sertifikasi </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tuk_kejuruan">TUK Kejuruan</label>
                                        <fieldset class="form-group">
                                            <select id="tuk_kejuruan" name="tuk_kejuruan" class="form-select" id="basicSelect">
                                                <option value="">-- Pilih TUK Kejuruan --</option>
                                                <?php foreach ($list_tuk as $row) { ?>
                                                    <option value="<?= $row->kode ?>"><?= $row->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tuk">TUK</label>
                                        <fieldset class="form-group">
                                            <select id="tuk" name="tuk" class="form-select">
                                                <option value="">-- Pilih TUK --</option>

                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="skema_kejuruan">Skema Kejuruan</label>
                                        <fieldset class="form-group">
                                            <select id="skema_kejuruan" name="skema_kejuruan" class="form-select">
                                                <option value="">-- Pilih Skema Kejuruan --</option>
                                                <?php foreach ($list_skema as $row) { ?>
                                                    <option value="<?= $row->kode ?>"><?= $row->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="skema">Skema</label>
                                        <fieldset class="form-group">
                                            <select id="skema" name="skema" class="form-select">
                                                <option value="">-- Pilih skema --</option>

                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="sumber_anggaran">Sumber Anggaran</label>
                                        <fieldset class="form-group">
                                            <select id="sumber_anggaran" name="sumber_anggaran" class="form-select">
                                                <option value="">-- Pilih Sumber Anggaran --</option>
                                                <?php foreach ($list_sumber_anggaran as $row) { ?>
                                                    <option value="<?= $row->id ?>"><?= $row->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="anggaran_perasesi">Anggaran Per Asesi</label>
                                        <input style="color:black" type="number" id="anggaran_perasesi" class="form-control" name="anggaran_perasesi" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_spk">Nomor SK UJK</label>
                                        <input style="color:black" type="number" id="nomor_spk" class="form-control" name="nomor_spk" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_spk">Tanggal Terbit SK UJK</label>
                                        <input style="color:black" type="date" id="tanggal_spk" class="form-control" name="tanggal_spk" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_jadwal">Nama Jadwal</label>
                                        <input style="color:black" type="text" id="nama_jadwal" class="form-control" name="nama_jadwal" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_uji">Tanggal Uji</label>
                                        <input style="color:black" type="date" id="tanggal_uji" class="form-control" name="tanggal_uji" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="target_peserta">Target Peserta</label>
                                        <input style="color:black" type="number" id="target_peserta" class="form-control" name="target_peserta" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="jam_uji">Jam Uji</label>
                                        <input style="color:black" type="number" id="jam_uji" class="form-control" name="jam_uji" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="lokasi_uji">Alamat Uji Kompetensi</label>
                                        <textarea style="color:black" rows="4" id="lokasi_uji" class="form-control" name="lokasi_uji"> </textarea>
                                    </div>
                                </div>


                            </div>

                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/manajemen_jadwal/jadwal_asesmen.js') }}"></script>


@endsection