@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">
                        <input type="hidden" id="id_surat_tugas" value="<?= $id_surat_tugas ?>">
                        <input type="hidden" id="hari_mentah" value="<?= $surat_tugas->tanggal_uji ?>">
                        <button onclick="printSuratTugas()" type="button" class="btn btn-outline-primary block">
                            Print Surat Tugas
                        </button>
                        <br>
                        <br>
                        <div class="card" id="viewSuratTugas">
                            <div class="card-content">
                                <div class="card-body">
                                    <h3 style="text-align:center;"><u>SURAT TUGAS ASESOR</u></h3>
                                    <h4 style="text-align:center;">Nomor. <span type="text" id="nomor_surat" disabled style="width:30%; text-align: center;"><?= $surat_tugas->nomor_surat ?></span> </h3>
                                        <br><br>
                                        <div class="table-responsive">
                                            <table id="tblViewSuratTugas" class="table">
                                                <tr style="border:white">
                                                    <td rowspan="2" width="100px">Pertimbangan</td>
                                                    <td>1.</td>
                                                    <td>Pelaksanaan Uji Kompetensi oleh TUK <span type="text" name="tuk1" id="tuk1" disabled> <?= $surat_tugas->tuk ?> </span>. </td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td>2.</td>
                                                    <td>Surat Keputusan Direktur Eksekutif LSP <span type="text" name="lsp1" id="lsp1" disabled> </span>
                                                        Nomor. <span type="text" name="nomor_Keputusan" id="nomor_Keputusan" disabled> <?= $surat_tugas->nomor_surat ?> </span>,
                                                        tanggal <span type="text" name="tanggal_Surat1" id="tanggal_Surat1" disabled> <?= $surat_tugas->tanggal_surat ?> </span>
                                                        tentang pelaksanaan Uji Kompetensi untuk Skema <span type="text" name="skema1" id="skema1" disabled> <?= $surat_tugas->skema ?> </span>
                                                        di TUK <span type="text" name="tuk2" id="tuk2" disabled> <?= $surat_tugas->tuk ?></span>
                                                        pada Pelaksanaan Sertifikasi Kompetensi Kerja BNSP tahun 2022.</td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td colspan="3"> </td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td rowspan="2">Dasar</td>
                                                    <td>1.</td>
                                                    <td>Perjanjian Pelaksanaan Sertifikasi Kompetensi Kerja antara Pejabat Pembuat Komitmen Sekretariat Badan Nasional Sertifikasi Profesi (BNSP) dengan Lembaga Sertifikasi Profesi <span type="text" name="lsp2" id="lsp2" disabled> </span>
                                                        selaku Pelaksanaan Sertifikasi Kompetensi Kerja, Nomor. <span type="text" name="nomor_spk" id="nomor_spk1" disabled> <?= $surat_tugas->nomor_spk ?> </span>,
                                                        tanggal <span type="text" name="tanggal_spk" id="tanggal_spk1" disabled> <?= $surat_tugas->tanggal_spk ?></span>.</td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td>2.</td>
                                                    <td>Surat Perintah Mulai Kerja Nomor. <span type="text" name="nomor_spk" id="nomor_spk2" disabled> <?= $surat_tugas->nomor_spk ?></span>,
                                                        tanggal <span type="text" name="tanggal_spk" id="tanggal_spk2" disabled> <?= $surat_tugas->tanggal_spk ?> </span>.</td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td colspan="3"> </td>
                                                </tr>
                                            </table>

                                            <div id="rAsesor"></div>

                                            <table class="table2">
                                                <tr style="border:white">
                                                    <td colspan="2" width="100px">Jabatan</td>
                                                    <td></td>
                                                    <td colspan="2">Asesor Kompetensi</td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td colspan="3"> </td>
                                                </tr>
                                                <tr style="border:white">
                                                    <td colspan="2" width="100px">Untuk</td>
                                                    <td width="10px">1.</td>
                                                    <td>Mengakses Skema <span type="text" name="skema2" id="skema2" disabled> <?= $surat_tugas->skema ?> </span> dengan unit kompetensi sebagai berikut :</td>
                                                </tr>
                                            </table>

                                            <div id="rUnit">
                                                <table class="tableS">
                                                    <?php foreach ($data_skema as $row) : ?>
                                                        <tr>
                                                            <td width="135px"> </td>
                                                            <td width="130px">
                                                                <output id="no_uk" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                            </td>
                                                            <td>
                                                                <output value="<?= $row->nama ?>" id="uk" class="bg2"><?= $row->nama ?></output>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </div>

                                            <table class="table3">
                                                <tr style="border:white;">
                                                    <td width="100px"></td>
                                                    <td>2.</td>
                                                    <td>Dalam melaksanakan tugasnya perlu memperhatikan hal-hal sebagai berikut:<br>
                                                        a. Data kelengkapan uji kompetensi;<br>
                                                        b. Uji kompetensi diadakan pada hari <span type="text" name="hari" id="hari" disabled> </span>, tanggal <span type="text" name="tanggal_Uji" id="tanggal_Uji" disabled> <?= $surat_tugas->tanggal_uji ?></span>
                                                        pada TUK <span type="text" name="tuk3" id="tuk3" disabled> <?= $surat_tugas->tuk ?> </span>
                                                        di alamat <span type="text" name="alamat" id="alamat" disabled> <?= $surat_tugas->lokasi ?></span>.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"></td>
                                                    <td>3.</td>
                                                    <td>Melaksanakan tugas dengan sebaik-baiknya dan melaporkan hasilnya kepada Direktur Eksekutif LSP <span type="text" name="lsp3" id="lsp3" disabled> </span>.</td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"></td>
                                                    <td>4.</td>
                                                    <td>Semua biaya yang timbul akibat dikeluarkannya Surat Perintah Tugas ini dibebankan kepada Anggaran Pelaksanaan Sertifikasi Kompetensi Kerja - BNSP Tahun 2022 melalui LSP <span type="text" name="lsp4" id="lsp4" disabled> </span>.</td>
                                                </tr>
                                            </table>

                                            <br><br><br>
                                            <table class="table4">
                                                <tr>
                                                    <td width="100px">Dikeluarkan_di</td>
                                                    <td width="10px">:</td>
                                                    <td><span type="text" name="lokasi" id="lokasi" disabled> <?= $surat_tugas->lokasi ?></span> </td>
                                                </tr>
                                                <tr>
                                                    <td>Pada tanggal</td>
                                                    <td>:</td>
                                                    <td><span type="text" name="tanggal_surat2" id="tanggal_surat2" disabled> <?= $surat_tugas->tanggal_surat ?></span> </td>
                                                </tr>
                                            </table>
                                            <br><br>
                                            <table class="table5">
                                                <tr>
                                                    <td>DIREKTUR EKSEKUTIF</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;">LSP <span type="text" name="lsp5" id="lsp5" disabled> </span> </td>
                                                </tr>
                                                <tr>
                                                    <td><br><br><br> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;">(<span type="text" name="nama_direktur" id="nama_direktur" disabled> <?= $direktur->nama ?></span>)</td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<script src="{{ asset('assets/js/manajemen_jadwal/view_surat_tugas.js') }}"></script>


@endsection