@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <?= $title_sub_menu; ?>
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tbl_monitoring_apl01" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>tuk</th>
                                                    <th>judul jadwal</th>
                                                    <th>tanggal asesmen</th>
                                                    <th>nama asesi</th>
                                                    <th>nik</th>
                                                    <th>tempat lahir</th>
                                                    <th>tanggal lahir</th>
                                                    <th>jenis kelamin</th>
                                                    <th>kebangsaan</th>
                                                    <th>alamat</th>
                                                    <th>kode pos</th>
                                                    <th>provinsi</th>
                                                    <th>kabupaten kota</th>
                                                    <th>telepon</th>
                                                    <th>email</th>
                                                    <th>kualifikasi pendidikan</th>
                                                    <th>pekerjaan</th>
                                                    <th>institusi perusahaan</th>
                                                    <th>jabatan</th>
                                                    <th>alamat kantor</th>
                                                    <th>kode pos2</th>
                                                    <th>telepon2</th>
                                                    <th>fax2</th>
                                                    <th>email2</th>
                                                    <th>skema</th>
                                                    <th>nomor</th>
                                                    <th>nama asesor</th>
                                                    <th>no Reg</th>
                                                    <th>tujuan asesmen</th>
                                                    <th>pas Foto</th>
                                                    <th>ktp</th>
                                                    <th>ijazah</th>
                                                    <th>sertifikasi pelatihan</th>
                                                    <th>cv</th>
                                                    <th>portofolio</th>
                                                    <th>rekomendasi</th>
                                                    <th>nama pemohon</th>
                                                    <th>ttd pemohon</th>
                                                    <th>tanggal pemohon</th>
                                                    <th>catatan admin</th>
                                                    <th>nama admin</th>
                                                    <th>ttd admin</th>
                                                    <th>tanggal admin</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data_apl01 as $row) : ?>
                                                    <tr>
                                                        <td><?= $row->tuk ?></td>
                                                        <td><?= $row->juduljadwal ?></td>
                                                        <td><?= $row->tanggalasesmen ?></td>
                                                        <td><?= $row->nama_asesi ?></td>
                                                        <td><?= $row->nik ?></td>
                                                        <td><?= $row->tempatlahir ?></td>
                                                        <td><?= $row->tanggallahir ?></td>
                                                        <td><?= $row->jeniskelamin ?></td>
                                                        <td><?= $row->kebangsaan ?></td>
                                                        <td><?= $row->alamat ?></td>
                                                        <td><?= $row->kodepos ?></td>
                                                        <td><?= $row->provinsi ?></td>
                                                        <td><?= $row->kabupatenkota ?></td>
                                                        <td><?= $row->telepon ?></td>
                                                        <td><?= $row->email ?></td>
                                                        <td><?= $row->kualifikasipendidikan ?></td>
                                                        <td><?= $row->pekerjaan ?></td>
                                                        <td><?= $row->namainstitusiperusahaan ?></td>
                                                        <td><?= $row->jabatan ?></td>
                                                        <td><?= $row->alamatkantor ?></td>
                                                        <td><?= $row->kodepos2 ?></td>
                                                        <td><?= $row->telepon2 ?></td>
                                                        <td><?= $row->fax2 ?></td>
                                                        <td><?= $row->email2 ?></td>
                                                        <td><?= $row->skema ?></td>
                                                        <td><?= $row->nomor ?></td>
                                                        <td><?= $row->namaasesor ?></td>
                                                        <td><?= $row->no_Reg ?></td>
                                                        <td><?= $row->tujuanasesmen ?></td>
                                                        <td><?= $row->pasFoto ?></td>
                                                        <td><?= $row->ktp ?></td>
                                                        <td><?= $row->ijazah ?></td>
                                                        <td><?= $row->sertifikasipelatihan ?></td>
                                                        <td><?= $row->cv ?></td>
                                                        <td><?= $row->portofolio ?></td>
                                                        <td><?= $row->rekomendasi ?></td>
                                                        <td><?= $row->namapemohon ?></td>
                                                        <td><?= $row->ttdpemohon ?></td>
                                                        <td><?= $row->tanggalpemohon ?></td>
                                                        <td><?= $row->catatanadmin ?></td>
                                                        <td><?= $row->namaadmin ?></td>
                                                        <td><?= $row->ttdadmin ?></td>
                                                        <td><?= $row->tanggaladmin ?></td>


                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1">Basic Modal</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Bonbon caramels muffin. Chocolate bar oat cake cookie pastry dragée pastry.
                    Carrot cake
                    chocolate tootsie roll chocolate bar candy canes biscuit.

                    Gummies bonbon apple pie fruitcake icing biscuit apple pie jelly-o sweet
                    roll. Toffee sugar
                    plum sugar plum jelly-o jujubes bonbon dessert carrot cake. Cookie dessert
                    tart muffin topping
                    donut icing fruitcake. Sweet roll cotton candy dragée danish Candy canes
                    chocolate bar cookie.
                    Gingerbread apple pie oat cake. Carrot cake fruitcake bear claw. Pastry
                    gummi bears
                    marshmallow jelly-o.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-bs-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>
                <button type="button" class="btn btn-primary ml-1" data-bs-dismiss="modal">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Accept</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tbl_monitoring_apl01').DataTable({
            dom: "Bfrtip"

        });
    });
</script>


@endsection