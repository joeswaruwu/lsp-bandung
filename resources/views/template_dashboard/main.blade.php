<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Mazer Admin Dashboard</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/css/bootstrap.css">

    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/vendors/iconly/bold.css">

    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/css/app.css">
    <link rel="shortcut icon" href="{{ asset('assets/template/mazer-main/dist') }}/assets/images/favicon.svg" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- js untuk select2  -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <label for="">LSP Bandung</label>


                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <?php

                        use Illuminate\Support\Facades\DB;

                        $menu = DB::table('master_menu')
                            ->join('master_users_akses_menu', 'master_menu.id_menu', '=', 'master_users_akses_menu.menu_id')
                            ->where('master_users_akses_menu.role_id', '=', $role_id)
                            ->orderByRaw('master_menu.id_menu ASC')
                            ->get();
                        ?>
                        <?php foreach ($menu as $m) : ?>
                            <li class="sidebar-title"><?= $m->nama_menu ?></li>
                            <?php
                            $menu_id = $m->id_menu;
                            $sub_menu = DB::table('master_sub_menu')
                                ->join('master_menu', 'master_menu.id_menu', '=', 'master_sub_menu.menu_id')
                                ->join('master_users_akses_submenu', 'master_users_akses_submenu.sub_menu_id', '=', 'master_sub_menu.id_master_sub_menu')
                                ->where('master_sub_menu.menu_id', '=', $menu_id)
                                ->where('master_users_akses_submenu.role_id', '=', $role_id)
                                ->get();

                            ?>
                            <?php foreach ($sub_menu as $sm) : ?>
                                <li class="sidebar-item  has-sub">
                                    <?php if ($sm->url == "") { ?>
                                        <a href="" class='sidebar-link'>
                                        <?php } else { ?>
                                            <a href="<?= ($sm->url) ?>" class='sidebar-link2'>
                                            <?php } ?>
                                            <i class="<?= $sm->icon; ?>"></i>
                                            <span class="ms-3"><?= $sm->title; ?></span>
                                            </a>
                                            <ul class="submenu ">
                                                <?php
                                                $sub_menu_id = $sm->id_master_sub_menu;
                                                $sub_sub_menu = DB::table('master_sub_sub_menu')
                                                    ->join('master_sub_menu', 'master_sub_menu.id_master_sub_menu', '=', 'master_sub_sub_menu.sub_menu_id')
                                                    ->join('master_users_akses_subsubmenu', 'master_users_akses_subsubmenu.subsub_menu_id', '=', 'master_sub_sub_menu.id_master_sub_sub_menu')
                                                    ->where('master_sub_sub_menu.sub_menu_id', '=', $sub_menu_id)
                                                    ->where('master_users_akses_subsubmenu.role_id', '=', $role_id)
                                                    ->get();
                                                ?>
                                                <?php foreach ($sub_sub_menu as $ssm) : ?>
                                                    <li class="submenu-item ">
                                                        <a href="<?= asset($ssm->url_subsub_menu) ?>"><?= $ssm->child_title; ?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                </li>
                            <?php endforeach; ?>
                            <hr>
                        <?php endforeach; ?>

                        <li class="sidebar-item  has-sub">
                            <a href="/logout" class='sidebar-link2'>
                                <i class="bi bi-box-arrow-right"></i>
                                <span class="ms-3">Keluar</span>
                            </a>
                        </li>

                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>

        <!-- AWAL CONTENT -->
        @include('sweetalert::alert')
        @yield('isiDashboard')
        <!-- AKHIR CONTENT -->



    </div>

    <script src="{{ asset('assets/template/mazer-main/dist') }}/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('assets/template/mazer-main/dist') }}/assets/js/bootstrap.bundle.min.js"></script>

    <script src="{{ asset('assets/template/mazer-main/dist') }}/assets/vendors/apexcharts/apexcharts.js"></script>
    <script src="{{ asset('assets/template/mazer-main/dist') }}/assets/js/pages/dashboard.js"></script>

    <script src="{{ asset('assets/template/mazer-main/dist') }}/assets/js/main.js"></script>
</body>

</html>