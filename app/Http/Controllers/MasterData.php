<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class MasterData extends Controller
{
    public function struktur_jabatan(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Struktur Jabatan LSP";
        $data['list_jabatan'] = DB::select('SELECT * FROM list_jabatan WHERE is_aktif = 1');

        $token = $data_session['token'];
        $data['data_struktur_jabatan_lsp'] = DB::select("SELECT * FROM `struktur_jabatan_lsp`");
        return view('master_data.struktur_jabatan_lsp', $data);
    }

    public function getSrukturJabatanLsp(Request $request)
    {
        $data = DB::select('SELECT *
                            FROM `struktur_jabatan_lsp`
                            JOIN `list_jabatan`
                            ON `struktur_jabatan_lsp`.`jabatan` = `list_jabatan`.`id`');
        echo json_encode($data);
    }

    public function saveStrukturJabatanLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $jabatan = $request->input('jabatan');
        $nama = $request->input('nama');
        $nip = $request->input('nip');
        $waktu_sekarang     = Date('d F Y');
        //jika ada tanda_tangan
        if ($request->hasFile('ttd')) {
            $filenameWithExt = $request->file('ttd')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttd')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttd = 'ttd' . '_' . $nip . '.' . $extension; // Filename To store
            $request->ttd->move(public_path('assets/document/tanda_tangan/struktur_jabatan'), $fileName_ttd);
        } else {
            $fileName_ttd = "";
        }

        $data = array(
            'jabatan' => $jabatan,
            'nama' => $nama,
            'nip' => $nip,
            'ttd' => $fileName_ttd,
            'created_at' => $waktu_sekarang,
            'updated_at' => ""
        );

        DB::table('struktur_jabatan_lsp')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([MasterData::class, 'struktur_jabatan']);
    }

    public function hapusStrukturJabatanLsp(Request $request)
    {
        $id = $request->input('id');
        $waktu_sekarang     = Date('d F Y');

        DB::delete('DELETE FROM struktur_jabatan_lsp WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataStrukturJabatanLsp(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM struktur_jabatan_lsp WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateStrukturJabatanLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('struktur_jabatan_lsp')->where('id', $id)->first();

        $jabatan = $request->input('jabatan');
        $nama = $request->input('nama');
        $nip = $request->input('nip');
        $waktu_sekarang     = Date('d F Y');
        //jika ada tanda_tangan
        if ($request->hasFile('ttd')) {
            $filenameWithExt = $request->file('ttd')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttd')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttd = 'ttd' . '_' . $nip . '.' . $extension; // Filename To store
            $request->ttd->move(public_path('assets/document/tanda_tangan/struktur_jabatan'), $fileName_ttd);
        } else {
            $fileName_ttd = $data->ttd;
        }

        $update =  DB::table('struktur_jabatan_lsp')
            ->where('id', $id)
            ->update([
                'jabatan' => $jabatan,
                'nama' => $nama,
                'nip' => $nip,
                'ttd' => $fileName_ttd,
                'updated_at' => $waktu_sekarang
            ]);

        echo json_encode($update);
    }

    //DAFTAR_ADMIN
    public function daftar_admin(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar Admin";
        $data['list_jenis_lsp'] = DB::select('SELECT * FROM list_jenis_lsp WHERE is_aktif = 1');

        $token = $data_session['token'];
        // $data['data_struktur_jabatan_lsp'] = DB::select("SELECT * FROM `struktur_jabatan_lsp`");
        return view('master_data.daftar_admin', $data);
    }

    public function getDaftarAdmin(Request $request)
    {
        $data = DB::select('SELECT *
                            FROM `users`
                            WHERE role_id = 2'); //2 adalah hak akses admin
        echo json_encode($data);
    }

    public function saveDaftarAdmin(Request $request)
    {
        //cek duplikat email
        $email = $request->input('email');
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        // @dd($cek);
        if ($cek == []) {
            //jika nggk duplikat maka insert
            $token = md5($email);
            //jika ada dokument SK_LSP
            if ($request->hasFile('dokumen_sk_lsp')) {
                $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
            } else {
                $fileName_dokumen_sk_lsp = "";
            }
            //jika ada dokument LISENSI_LSP
            if ($request->hasFile('dokumen_lisensi_lsp')) {
                $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
            } else {
                $fileName_dokumen_lisensi_lsp = "";
            }
            //jika ada dokument NPWP
            if ($request->hasFile('dokumen_npwp')) {
                $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
            } else {
                $fileName_dokumen_npwp = "";
            }
            //jika ada dokument dokumen_rek_bank
            if ($request->hasFile('dokumen_rek_bank')) {
                $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
            } else {
                $fileName_dokumen_rek_bank = "";
            }

            $jenis_lsp = $request->input('jenis_lsp');
            $no_hp = $request->input('no_hp');
            $nama = $request->input('nama');
            $no_hp_kantor = $request->input('no_hp_kantor');
            $no_fax = $request->input('no_fax');
            $email = $request->input('email');
            $password = $request->input('password');
            $website = $request->input('website');
            $kode_pos = $request->input('kode_pos');
            $alamat_kantor = $request->input('alamat_kantor');
            $no_sk_lsp = $request->input('no_sk_lsp');
            $no_lisensi_lsp = $request->input('no_lisensi_lsp');
            $npwp = $request->input('npwp');
            $rekening_bank = $request->input('rekening_bank');
            $waktu_sekarang     = Date('d F Y');

            $data = array(
                'role_id' => 2,
                'name' => $nama,
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'token' => $token,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'alamat_kantor' => $alamat_kantor,
                'email' => $email,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'created_at' => $waktu_sekarang,
            );

            DB::table('users')->insert($data);

            Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
            return redirect()->action([MasterData::class, 'daftar_admin']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Email Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_admin']);
        }
    }

    public function hapusDaftarAdmin(Request $request)
    {
        $id = $request->input('id');
        $waktu_sekarang     = Date('d F Y');

        DB::delete('DELETE FROM users WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataDaftarAdmin(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM users WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateDataDaftarAdmin(Request $request)
    {

        $id = $request->input('id');
        $data = DB::table('users')->where('id', $id)->first();

        $data_session = $request->session()->get('dataUser');
        $token = $data->token;

        //jika ada dokument SK_LSP
        if ($request->hasFile('dokumen_sk_lsp')) {
            $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
        } else {
            $fileName_dokumen_sk_lsp = $data->dokumen_sk_lsp;
        }
        //jika ada dokument LISENSI_LSP
        if ($request->hasFile('dokumen_lisensi_lsp')) {
            $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
        } else {
            $fileName_dokumen_lisensi_lsp = $data->dokumen_lisensi_lsp;
        }
        //jika ada dokument NPWP
        if ($request->hasFile('dokumen_npwp')) {
            $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
        } else {
            $fileName_dokumen_npwp = $data->dokumen_npwp;
        }
        //jika ada dokument dokumen_rek_bank
        if ($request->hasFile('dokumen_rek_bank')) {
            $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
        } else {
            $fileName_dokumen_rek_bank = $data->dokumen_rek_bank;
        }

        $nama = $request->input('nama');
        $jenis_lsp = $request->input('jenis_lsp');
        $no_hp = $request->input('no_hp');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $no_fax = $request->input('no_fax');
        $email = $request->input('email');
        $password = $request->input('password');
        $website = $request->input('website');
        $kode_pos = $request->input('kode_pos');
        $alamat_kantor = $request->input('alamat_kantor');
        $no_sk_lsp = $request->input('no_sk_lsp');
        $no_lisensi_lsp = $request->input('no_lisensi_lsp');
        $npwp = $request->input('npwp');
        $rekening_bank = $request->input('rekening_bank');
        $waktu_sekarang     = Date('d F Y');

        $update =  DB::table('users')
            ->where('token', $token)
            ->update([
                'name' => $nama,
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'email' => $email,
                'alamat_kantor' => $alamat_kantor,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'updated_at' => $waktu_sekarang,
            ]);
        echo json_encode($update);
    }

    //daftar_penyelia
    public function daftar_penyelia(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar Penyelia";
        $data['list_akses'] = DB::select('SELECT * FROM master_users_role WHERE id = 5');
        $data['provinsi'] = DB::select('SELECT * FROM wilayah_2020 WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');

        $token = $data_session['token'];
        return view('master_data.daftar_penyelia', $data);
    }

    public function getDaftarPenyelia(Request $request)
    {
        $data = DB::select('SELECT
                            `a`.`id` as `id`,
                            `a`.`email` as `email`,
                            `a`.`password` as `password`,
                            `a`.`name` as `name`,
                            `b`.`nama_role` as `nama_role`,
                            `a`.`alamat` as `alamat`,
                            `a`.`kodepos` as `kodepos`,
                            `c`.`nama` as `nama_provinsi`
                            FROM `users` `a`
                            JOIN `master_users_role` `b`
                            ON `a`.`role_id` = `b`.`id`
                            JOIN `wilayah_2020` `c`
                            ON `c`.`kode` = `a`.`provinsi`
                            WHERE `a`.`role_id` = 5'); //5 adalah hak akses penyelia
        echo json_encode($data);
    }

    public function saveDaftarPenyelia(Request $request)
    {
        //cek duplikat email
        $email = $request->input('email');
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        // @dd($cek);
        if ($cek == []) {
            //jika nggk duplikat maka insert
            $token = md5($email);
            $email = $request->input('email');
            $password = $request->input('password');
            $nama = $request->input('nama');
            $akses = $request->input('akses');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $provinsi = $request->input('provinsi');
            $kab_kota = $request->input('kab_kota');
            $waktu_sekarang     = Date('d F Y');

            $data = array(
                'token' => $token,
                'email' => $email,
                'password' => $password,
                'name' => $nama,
                'role_id' => $akses,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'created_at' => $waktu_sekarang,
            );

            DB::table('users')->insert($data);

            Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
            return redirect()->action([MasterData::class, 'daftar_penyelia']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Email Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_penyelia']);
        }
    }

    public function hapusDaftarPenyelia(Request $request)
    {
        $id = $request->input('id');
        DB::delete('DELETE FROM users WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataDaftarPenyelia(Request $request)
    {
        $id = $request->input('id');
        $dataPenyelia = DB::table('users')->where('id', $id)->first();
        $kab_kota_id = $dataPenyelia->kabupaten_kota;
        $dataKabKota = DB::table('wilayah_2020')->where('kode', $kab_kota_id)->first();
        $data = [
            'dataPenyelia' => $dataPenyelia,
            'dataKabKota' => $dataKabKota
        ];
        echo json_encode($data);
    }

    public function updateDataDaftarPenyelia(Request $request)
    {
        //jika nggk duplikat maka insert
        $id = $request->input('id');
        $email = $request->input('email');
        $password = $request->input('password');
        $nama = $request->input('nama');
        $akses = $request->input('akses');
        $alamat = $request->input('alamat');
        $kode_pos = $request->input('kode_pos');
        $provinsi = $request->input('provinsi');
        $kab_kota = $request->input('kab_kota');
        $waktu_sekarang     = Date('d F Y');

        $update =  DB::table('users')
            ->where('id', $id)
            ->update([
                'email' => $email,
                'password' => $password,
                'name' => $nama,
                'role_id' => $akses,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'updated_at' => $waktu_sekarang
            ]);
        echo json_encode($update);
    }
}
