<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Auth extends Controller
{

    public function login()
    {
        return view('auth.login');
    }

    public function validasiLogin(Request $request)
    {
        $user = DB::table('users')->where('email', $request->input('email'))->first();
        if ($user) {
            //cek pin
            if ($user->password == $request->input('password')) {
                $request->session()->put('pesan', 'Berhasil login');
                $dataUser = [
                    'name'     => $user->name,
                    'email'     => $user->email,
                    'token'     => $user->token,
                    'foto_profile'     => $user->foto_profile,
                    'password'  => $user->password,
                    'role_id'  => $user->role_id
                ];
                $request->session()->put('dataUser', $dataUser);
                if ($dataUser['role_id'] == 1) {
                    return redirect()->action([Sa::class, 'sa']);
                } else if ($dataUser['role_id'] == 2) {
                    return redirect()->action([Admin::class, 'admin']);
                } else if ($dataUser['role_id'] == 3) {
                    return redirect()->action([Asesor::class, 'asesor']);
                } else if ($dataUser['role_id'] == 4) {
                    return redirect()->action([Asesi::class, 'asesi']);
                } else if ($dataUser['role_id'] == 5) {
                    return redirect()->action([Penyelia::class, 'penyelia']);
                }
            } else {
                Alert::error('Gagal', 'Password Anda Salah');
                return redirect()->action([Auth::class, 'login']);
            }
        } else {
            Alert::error('Gagal', 'Email Anda tidak Terdaftar');
            return redirect()->action([Auth::class, 'login']);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('dataUser');

        return redirect('/');
    }


    // public function daftar()
    // {
    //     return view('auth.daftar');
    // }

    // public function AksiPendaftaranManual(Request $request)
    // {
    //     $waktu_sekarang     = Date('d F Y');

    //     $data = DB::table('users')->insert([
    //         'role_id' => '2',
    //         'name' => $request->input('name'),
    //         'email' => $request->input('email'),
    //         'foto_profile' => 'default.jpg',
    //         'email_verified_at' => null,
    //         'password' => $request->input('password'),
    //         'no_hp' => $request->input('no_hp'),
    //         'jenis_kelamin' => $request->input('jenis_kelamin'),
    //         'remember_token' => null,
    //         'created_at' => $waktu_sekarang,
    //         'updated_at' => '',
    //         'deleted_at' => ''
    //     ]);

    //     echo json_encode($data);
    // }


    // public function tampilan_berhasil_daftar()
    // {
    //     var_dump("berhasil_daftar");
    //     die;
    // }


}
