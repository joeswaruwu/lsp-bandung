<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Asesi extends Controller
{
    public function asesi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Dashboard";
        return view('dashboard.asesi', $data);
    }

    public function profile_asesi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();

        $data['jenis_kelamin'] = DB::select('SELECT * FROM list_kelamin');
        $data['kebangsaan'] = DB::select('SELECT * FROM list_kebangsaan');
        $data['pendidikan'] = DB::select('SELECT * FROM list_pendidikan');
        $data['pekerjaan'] = DB::select('SELECT * FROM list_pekerjaan');
        $data['provinsi'] = DB::select('SELECT * FROM wilayah_2020 WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
        $kode_kab_kota = $data['user']->kabupaten_kota;
        $data['kabupaten_kota'] = DB::select("SELECT * FROM wilayah_2020 WHERE kode = '$kode_kab_kota'");

        // @dd($data['provinsi']);

        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Profile Asesi";
        return view('asesi.profile_asesi', $data);
    }

    public function saveProfileAsesi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        //jika ada tanda_tangan asesi
        if ($request->hasFile('tanda_tangan')) {
            $filenameWithExt = $request->file('tanda_tangan')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('tanda_tangan')->getClientOriginalExtension(); // Get just Extension
            $fileName_tanda_tangan = 'ttd_asesi' . '_' . $token . '.' . $extension; // Filename To store
            $request->tanda_tangan->move(public_path('assets/document/tanda_tangan/asesi'), $fileName_tanda_tangan);
        } else {
            $fileName_tanda_tangan = $user->tanda_tangan;
        }


        $email = $request->input('email');
        $password = $request->input('password');
        $name = $request->input('name');
        $nik = $request->input('nik');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $jenis_kelamin = $request->input('jenis_kelamin');
        $kebangsaan = $request->input('kebangsaan');
        $alamat = $request->input('alamat');
        $kode_pos = $request->input('kode_pos');
        $pekerjaan = $request->input('pekerjaan');
        $provinsi = $request->input('provinsi');
        $kab_kota = $request->input('kab_kota');
        $no_hp = $request->input('no_hp');
        $pendidikan = $request->input('pendidikan');
        $institusi_perusahaan = $request->input('institusi_perusahaan');
        $jabatan = $request->input('jabatan');
        $alamat_kantor = $request->input('alamat_kantor');
        $kode_pos_kantor = $request->input('kode_pos_kantor');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $fax_kantor = $request->input('fax_kantor');
        $email_kantor = $request->input('email_kantor');
        $waktu_sekarang     = Date('d F Y');

        $update_users =  DB::table('users')
            ->where('token', $token)
            ->update([
                'email' => $email,
                'password' => $password,
                'name' => $name,
                'nik' => $nik,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'kebangsaan' => $kebangsaan,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'no_hp' => $no_hp,
                'kualifikasi_pendidikan' => $pendidikan,
                'pekerjaan' => $pekerjaan,
                'tanda_tangan' => $fileName_tanda_tangan,
                'institusi_perusahaan' => $institusi_perusahaan,
                'jabatan' => $jabatan,
                'alamat_kantor' => $alamat_kantor,
                'kodepos_kantor' => $kode_pos_kantor,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $fax_kantor,
                'email_kantor' => $email_kantor,
                'updated_at' => $waktu_sekarang
            ]);
        Alert::success('Berhasil', 'Data Berhasil di Update');
        return redirect()->action([Asesi::class, 'profile_asesi']);
    }
}
