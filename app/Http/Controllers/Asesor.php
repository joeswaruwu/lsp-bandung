<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Asesor extends Controller
{
    public function asesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Dashboard";
        return view('dashboard.asesor', $data);
    }

    public function profile_asesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();

        $data['jenis_kelamin'] = DB::select('SELECT * FROM list_kelamin');
        $data['kebangsaan'] = DB::select('SELECT * FROM list_kebangsaan');
        $data['pendidikan'] = DB::select('SELECT * FROM list_pendidikan');
        $data['pekerjaan'] = DB::select('SELECT * FROM list_pekerjaan');
        $data['provinsi'] = DB::select('SELECT * FROM wilayah_2020 WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
        $kode_kab_kota = $data['user']->kabupaten_kota;
        $data['kabupaten_kota'] = DB::select("SELECT * FROM wilayah_2020 WHERE kode = '$kode_kab_kota'");

        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Profile Asesor";
        return view('asesor.profile_asesor', $data);
    }

    public function saveProfileAsesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        //jika ada tanda_tangan asesi
        if ($request->hasFile('tanda_tangan')) {
            $filenameWithExt = $request->file('tanda_tangan')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('tanda_tangan')->getClientOriginalExtension(); // Get just Extension
            $fileName_tanda_tangan = 'ttd_asesor' . '_' . $token . '.' . $extension; // Filename To store
            $request->tanda_tangan->move(public_path('assets/document/tanda_tangan/asesor'), $fileName_tanda_tangan);
        } else {
            $fileName_tanda_tangan = $user->tanda_tangan;
        }

        //jika ada sertif_asesor
        if ($request->hasFile('sertif_asesor')) {
            $filenameWithExt = $request->file('sertif_asesor')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('sertif_asesor')->getClientOriginalExtension(); // Get just Extension
            $fileName_sertif_asesor = 'sertif_asesor' . '_' . $token . '.' . $extension; // Filename To store
            $request->sertif_asesor->move(public_path('assets/document/sertif_asesor'), $fileName_sertif_asesor);
        } else {
            $fileName_sertif_asesor = $user->sertif_asesor;
        }

        //jika ada sertif_teknis
        if ($request->hasFile('sertif_teknis')) {
            $filenameWithExt = $request->file('sertif_teknis')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('sertif_teknis')->getClientOriginalExtension(); // Get just Extension
            $fileName_sertif_teknis = 'sertif_teknis' . '_' . $token . '.' . $extension; // Filename To store
            $request->sertif_teknis->move(public_path('assets/document/sertif_teknis'), $fileName_sertif_teknis);
        } else {
            $fileName_sertif_teknis = $user->sertif_teknis;
        }


        $name = $request->input('name');
        $email = $request->input('email');
        $nik = $request->input('nik');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $jenis_kelamin = $request->input('jenis_kelamin');
        $kebangsaan = $request->input('kebangsaan');
        $alamat = $request->input('alamat');
        $kode_pos = $request->input('kode_pos');
        $provinsi = $request->input('provinsi');
        $kab_kota = $request->input('kab_kota');
        $no_hp = $request->input('no_hp');
        $pendidikan = $request->input('pendidikan');
        $pekerjaan = $request->input('pekerjaan');
        $no_reg = $request->input('no_reg');
        $no_sertif = $request->input('no_sertif');
        $no_blanko = $request->input('no_blanko');
        $tgl_sertif = $request->input('tgl_sertif');
        $tgl_expired = $request->input('tgl_expired');
        $waktu_sekarang     = Date('d F Y');

        $update_users =  DB::table('users')
            ->where('token', $token)
            ->update([
                'name' => $name,
                'email' => $email,
                'nik' => $nik,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'kebangsaan' => $kebangsaan,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'no_hp' => $no_hp,
                'kualifikasi_pendidikan' => $pendidikan,
                'pekerjaan' => $pekerjaan,
                'tanda_tangan' => $fileName_tanda_tangan,
                'no_reg' => $no_reg,
                'no_sertif' => $no_sertif,
                'no_blanko' => $no_blanko,
                'tgl_sertif' => $tgl_sertif,
                'tgl_expired' => $tgl_expired,
                'sertif_asesor' => $fileName_sertif_asesor,
                'sertif_teknis' => $fileName_sertif_teknis,
                'updated_at' => $waktu_sekarang
            ]);
        Alert::success('Berhasil', 'Data Berhasil di Update');
        return redirect()->action([Asesor::class, 'profile_asesor']);
    }
}
