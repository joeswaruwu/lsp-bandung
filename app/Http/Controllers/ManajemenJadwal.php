<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ManajemenJadwal extends Controller
{
    public function jadwal_asesmen(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesmen";
        $token = $data_session['token'];

        $data['list_tuk'] = DB::select('SELECT * FROM list_tuk WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
        $data['list_skema'] = DB::select('SELECT * FROM list_skema WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
        $data['list_sumber_anggaran'] = DB::select('SELECT * FROM list_sumber_anggaran');
        return view('manajemen_jadwal.jadwal_asesmen', $data);
    }

    public function getJadwalAsesmen(Request $request)
    {
        $data = DB::select('SELECT * FROM `jadwal_Asesmen` ORDER BY tanggal_Uji DESC');
        echo json_encode($data);
    }

    public function getSubTuk(Request $request)
    {
        $valTukKejuruan = $request->input('valTukKejuruan');
        $n = strlen($valTukKejuruan);
        $data = DB::select("SELECT * FROM list_tuk WHERE LEFT(kode,$n)='$valTukKejuruan' AND CHAR_LENGTH(kode)=6 ORDER BY nama");
        echo json_encode($data);
    }

    public function getSubSkema(Request $request)
    {
        $valSkemaKejuruan = $request->input('valSkemaKejuruan');
        $n = strlen($valSkemaKejuruan);

        $data = DB::select("SELECT * FROM list_skema WHERE LEFT(kode,$n)='$valSkemaKejuruan' AND CHAR_LENGTH(kode)=5 ORDER BY nama");
        echo json_encode($data);
    }

    public function saveJadwalAsesmen(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $nama_pelatihan = $request->input('nama_pelatihan');
        $tanggal_mulai_pelatihan = $request->input('tanggal_mulai_pelatihan');
        $tanggal_akhir_pelatihan = $request->input('tanggal_akhir_pelatihan');
        $nama_instansi = $request->input('nama_instansi');
        $durasi_pelatihan = $request->input('durasi_pelatihan');
        $tuk_kejuruan = $request->input('tuk_kejuruan');
        $tuk = $request->input('tuk');
        $skema_kejuruan = $request->input('skema_kejuruan');
        $skema = $request->input('skema');
        $sumber_anggaran = $request->input('sumber_anggaran');
        $anggaran_perasesi = $request->input('anggaran_perasesi');
        $nomor_spk = $request->input('nomor_spk');
        $tanggal_spk = $request->input('tanggal_spk');
        $nama_jadwal = $request->input('nama_jadwal');
        $tanggal_uji = $request->input('tanggal_uji');
        $target_peserta = $request->input('target_peserta');
        $jam_uji = $request->input('jam_uji');
        $lokasi_uji = $request->input('lokasi_uji');
        $waktu_sekarang     = Date('d F Y');


        $data = array(
            'tanggal_uji' => $tanggal_uji,
            'nama_jadwal' => $nama_jadwal,
            'tuk' => $tuk,
            'tema' => "$skema_kejuruan",
            'skema' => $skema,
            'nomor' => "",
            'id_skema' => "",
            'target_peserta' => $target_peserta,
            'jam_uji' => $jam_uji,
            'lokasi_uji' => $lokasi_uji,
            'sumber_anggaran' => $sumber_anggaran,
            'anggaran_perasesi' => $anggaran_perasesi,
            'nomor_spk' => $nomor_spk,
            'tanggal_spk' => $tanggal_spk,
            'tgl_mulai_pelatihan' => $tanggal_mulai_pelatihan,
            'tgl_akhir_pelatihan' => $tanggal_akhir_pelatihan,
            'durasi_pelatihan' => $durasi_pelatihan,
            'nama_pelatihan' => $nama_pelatihan,
            'nama_instansi' => $nama_instansi,
            'nama_lsp' => "BBPVP_Bandung"
        );

        DB::table('jadwal_asesmen')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'jadwal_asesmen']);
    }

    public function hapusJadwalAsesmen(Request $request)
    {
        $id = $request->input('id');

        DB::delete('DELETE FROM jadwal_asesmen WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function jadwalLanjutTuk(Request $request, string $id)
    {
        // id jadwal_asesmen = $id
        //ambil tanggal dari jadwal asesmen
        $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        $tanggal_uji = $res_jadwal_asesmen->tanggal_uji;
        $nama_jadwal = $res_jadwal_asesmen->nama_jadwal;
        $data['jadwal_asesor'] = DB::select("SELECT * FROM `jadwal_asesor` WHERE nama_jadwal='$nama_jadwal' AND tanggal_uji='$tanggal_uji' ORDER BY asesor ASC");

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";

        return view('manajemen_jadwal.jadwal_lanjut_tuk', $data);
    }

    public function penugasan_asesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";

        $data['nama_jadwal'] = DB::select("SELECT id, nama_jadwal, tanggal_uji FROM jadwal_asesmen WHERE tanggal_uji>=CURRENT_DATE");
        $data['asesor'] = DB::select("SELECT `id`, `name`, `no_reg` FROM `users` WHERE `role_id`=3 AND NOT `no_reg`='' ORDER BY `name`");
        return view('manajemen_jadwal.penugasan_asesor', $data);
    }

    public function getTanggalUjiJadwalAsesmen(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM jadwal_asesmen WHERE id = '$id'");
        echo json_encode($data);
    }

    public function getNoregAsesor(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM `users` WHERE `id` = '$id'");
        echo json_encode($data);
    }

    public function saveJadwalAsesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $id_nama_jadwal = $request->input('nama_jadwal');
        $data_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id_nama_jadwal)->first();
        $nama_jadwal = $data_jadwal_asesmen->nama_jadwal;
        $tanggal_uji = $request->input('tanggal_uji');
        $id_asesor = $request->input('asesor');
        $data_users = DB::table('users')->where('id', $id_asesor)->first();
        $nama_asesor = $data_users->name;
        $no_reg = $request->input('no_reg');
        $jumlah_asesi = $request->input('jumlah_asesi');
        $waktu_sekarang     = Date('d F Y');


        $data = array(
            'nama_jadwal' => $nama_jadwal,
            'tanggal_uji' => $tanggal_uji,
            'asesor' => $nama_asesor,
            'no_reg' => "$no_reg",
            'jumlah_asesi' => $jumlah_asesi,
            'nama_lsp' => "BBPVP_Bandung"
        );

        DB::table('jadwal_asesor')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'penugasan_asesor']);
    }

    public function surat_tugas(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Surat Tugas Asesor";

        $data['nama_jadwal'] = DB::select("SELECT id, nama_jadwal, tanggal_uji FROM jadwal_asesmen WHERE tanggal_uji>=CURRENT_DATE");
        // $data['asesor'] = DB::select("SELECT `id`, `name`, `no_reg` FROM `users` WHERE `role_id`=3 AND NOT `no_reg`='' ORDER BY `name`");
        return view('manajemen_jadwal.surat_tugas', $data);
    }

    public function getSuratTugas(Request $request)
    {
        $data = DB::select('SELECT * FROM surat_tugas ORDER BY id DESC');
        echo json_encode($data);
    }

    public function saveSuratTugas(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $id_nama_jadwal = $request->input('nama_jadwal');
        $data_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id_nama_jadwal)->first();
        $nama_jadwal = $data_jadwal_asesmen->nama_jadwal;
        $skema = $request->input('skema');
        $sumber_anggaran = $request->input('sumber_dana');
        $tuk = $request->input('nama_tuk');
        $tanggal_uji = $request->input('tanggal_uji');
        $lokasi = $request->input('lokasi');
        $tanggal_surat = $request->input('tanggal_surat');
        $nomor_surat = $request->input('nomor_surat');
        $nomor_keputusan = $request->input('nomor_keputusan');
        $nomor_spk = $request->input('nomor_spk');
        $tanggal_spk = $request->input('tanggal_spk');
        $waktu_sekarang     = Date('d F Y');


        $data = array(
            'tanggal_uji' => $tanggal_uji,
            'nama_jadwal' => $nama_jadwal,
            'tuk' => $tuk,
            'skema' => $skema,
            'id_skema' => "",
            'sumber_anggaran' => $sumber_anggaran,
            'nomor_spk' => $nomor_spk,
            'tanggal_spk' => $tanggal_spk,
            'tanggal_surat' => "$tanggal_surat",
            'lokasi' => $lokasi,
            'nomor_surat' => "$nomor_surat",
            'nomor_keputusan' => "$nomor_keputusan"
        );

        DB::table('surat_tugas')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'surat_tugas']);
    }

    public function view_surat_tugas(Request $request, string $id)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "View Surat Tugas";

        $data['id_surat_tugas'] = $id;
        $data['surat_tugas'] = DB::table('surat_tugas')->where('id', $id)->first();

        $data['data_skema'] = DB::select("SELECT id,induk,nama,
        max(case when (nama='1') then isi else '-' end) as kode,
        max(case when (nama='2') then isi else '-' end) as nama,
        max(case when (nama='3') then isi else '-' end) as tipe,
        max(case when (nama='4') then isi else '-' end) as elemen,
        max(case when (nama='5') then isi else '-' end) as kuk
        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
        (SELECT @pv := $id) b
        WHERE find_in_set(induk, @pv) AND length(@pv := concat(@pv, ',', id))
        group by grup,induk order by id ASC");
        // @dd($data['data_skema']);

        $data['direktur'] =  DB::table('struktur_jabatan_lsp')->where('jabatan', 1)->first();
        return view('manajemen_jadwal.view_surat_tugas', $data);
    }
}
