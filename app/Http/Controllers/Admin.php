<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;



class Admin extends Controller
{
    public function admin(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();
        $data['title_menu'] = "Dashboard";
        return view('dashboard.admin', $data);
    }

    public function profile_lsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();
        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Profile LSP";
        return view('admin.profile_lsp', $data);
    }

    public function saveProfileLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        //jika ada dokument SK_LSP
        if ($request->hasFile('dokumen_sk_lsp')) {
            $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
        } else {
            $fileName_dokumen_sk_lsp = $user->dokumen_sk_lsp;
        }
        //jika ada dokument LISENSI_LSP
        if ($request->hasFile('dokumen_lisensi_lsp')) {
            $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
        } else {
            $fileName_dokumen_lisensi_lsp = $user->dokumen_lisensi_lsp;
        }
        //jika ada dokument NPWP
        if ($request->hasFile('dokumen_npwp')) {
            $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
        } else {
            $fileName_dokumen_npwp = $user->dokumen_npwp;
        }
        //jika ada dokument dokumen_rek_bank
        if ($request->hasFile('dokumen_rek_bank')) {
            $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
        } else {
            $fileName_dokumen_rek_bank = $user->dokumen_rek_bank;
        }

        $jenis_lsp = $request->input('jenis_lsp');
        $no_hp = $request->input('no_hp');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $no_fax = $request->input('no_fax');
        $email = $request->input('email');
        $password = $request->input('password');
        $website = $request->input('website');
        $kode_pos = $request->input('kode_pos');
        $alamat_kantor = $request->input('alamat_kantor');
        $no_sk_lsp = $request->input('no_sk_lsp');
        $no_lisensi_lsp = $request->input('no_lisensi_lsp');
        $npwp = $request->input('npwp');
        $rekening_bank = $request->input('rekening_bank');
        $waktu_sekarang     = Date('d F Y');

        $update_users =  DB::table('users')
            ->where('token', $token)
            ->update([
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'email' => $email,
                'alamat_kantor' => $alamat_kantor,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'updated_at' => $waktu_sekarang,
            ]);
        Alert::success('Berhasil', 'Data Berhasil di Update');
        return redirect()->action([Admin::class, 'profile_lsp']);
    }
}
