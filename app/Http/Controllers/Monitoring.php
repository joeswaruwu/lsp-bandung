<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Monitoring extends Controller
{
    public function monitoring_apl01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl01";
        $token = $data_session['token'];
        $data['data_apl01'] = DB::select("SELECT * FROM `apl01` WHERE `token_asesor` = '$token'");
        return view('monitoring.monitoring_apl01', $data);
    }
}
