<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class UjiKompetensi extends Controller
{
    public function pskapl01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Permohonan Sertifikasi Kompetensi";
        $token = $data_session['token'];
        return view('uji_kompetensi.pskapl01', $data);
    }

    public function getJadwalAsesmen_pskapl01(Request $request)
    {
        $data = DB::select('SELECT * FROM jadwal_asesmen WHERE tanggal_uji>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY tanggal_uji DESC');
        echo json_encode($data);
    }

    public function jadwalLanjut(Request $request, string $id)
    {
        //ambil tanggal dari jadwal asesmen
        $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        $data['id_jadwal_asesmen'] = $id;
        //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        $tanggal_uji = $res_jadwal_asesmen->tanggal_uji;
        $nama_jadwal = $res_jadwal_asesmen->nama_jadwal;
        $data['jadwal_asesor'] = DB::select("SELECT * from jadwal_asesor WHERE nama_jadwal='$nama_jadwal' AND tanggal_uji='$tanggal_uji' ORDER BY asesor DESC");
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Pilih Asesor";

        return view('uji_kompetensi.jadwal_lanjut', $data);
    }

    public function apl01lsp(Request $request, string $id_asesor, string $id_jadwal_asesmen)
    {
        //ambil tanggal dari jadwal asesor
        $data['res_jadwal_asesor'] = DB::table('jadwal_asesor')->where('id', $id_asesor)->first();
        //ambil tanggal dari jadwal asesmen
        $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen']->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                        max(case when (nama='1') then isi else '-' end) as kode,
                                        max(case when (nama='2') then isi else '-' end) as nama

                                        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                        (SELECT @pv := $id_skema) b
                                        WHERE find_in_set(induk, @pv)
                                        group by grup,induk order by id ASC");
        // var_dump($res_jadwal_asesor);
        // var_dump($res_jadwal_asesmen);
        // die;
        // //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        // $tanggal_uji = $res_jadwal_asesmen->tanggal_uji;
        // $nama_jadwal = $res_jadwal_asesmen->nama_jadwal;
        // $data['jadwal_asesor'] = DB::select("SELECT * from jadwal_asesor WHERE nama_jadwal='$nama_jadwal' AND tanggal_uji='$tanggal_uji' ORDER BY asesor DESC");
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        // $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['user'] = DB::select("SELECT
                                    `a`.`name` AS `nama_lengkap`,
                                    `a`.`nik` AS `nik`,
                                    `a`.`tempat_lahir` AS `tempat_lahir`,
                                    `a`.`tanggal_lahir` AS `tanggal_lahir`,
                                    `b`.`nama` AS `nama_jenis_kelamin`,
                                    `c`.`nama` AS `nama_kebangsaan`,
                                    `a`.`alamat` AS `alamat`,
                                    `a`.`kodepos` AS `kodepos`,
                                    `d`.`nama` AS `nama_provinsi`,
                                    `a`.`kabupaten_kota` AS `kabupaten_kota`,
                                    `a`.`no_hp` AS `no_hp`,
                                    `a`.`email` AS `email`,
                                    `e`.`nama` AS `nama_kualifikasi_pendidikan`,
                                    `f`.`nama` AS `nama_pekerjaan`,
                                    `a`.`institusi_perusahaan` AS `institusi_perusahaan`,
                                    `a`.`jabatan` AS `jabatan`,
                                    `a`.`alamat_kantor` AS `alamat_kantor`,
                                    `a`.`kodepos_kantor` AS `kodepos_kantor`,
                                    `a`.`no_hp_kantor` AS `no_hp_kantor`,
                                    `a`.`fax_kantor` AS `fax_kantor`,
                                    `a`.`email_kantor` AS `email_kantor`,
                                    `a`.`tanda_tangan` AS `tanda_tangan`
                                    FROM `users` `a`
                                    JOIN `list_kelamin` `b`
                                    ON `a`.`jenis_kelamin` = `b`.`id`
                                    JOIN `list_kebangsaan` `c`
                                    ON `a`.`kebangsaan` = `c`.`id`
                                    JOIN `wilayah_2020` `d`
                                    ON `a`.`provinsi` = `d`.`kode`
                                    JOIN `list_pendidikan` `e`
                                    ON `a`.`kualifikasi_pendidikan` = `e`.`id`
                                    JOIN `list_pekerjaan` `f`
                                    ON `a`.`pekerjaan` = `f`.`id`
                                    WHERE `a`.`email` = '$email'
                                    ");
        $data['kab_kota_user'] = DB::select("SELECT
                                    `b`.`nama` AS `nama_kab_kota`
                                    FROM `users` `a`
                                    JOIN `wilayah_2020` `b`
                                    ON `a`.`kabupaten_kota` = `b`.`kode`
                                    WHERE `a`.`email` = '$email' ");
        // @dd($data['user']);
        // die;
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Form Apl 01";

        return view('uji_kompetensi.apl01lsp', $data);
    }

    // MENU BANDING ASESMEN (AK 04)
    public function ba04(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Banding Asesmen (AK 04)";
        $token = $data_session['token'];
        return view('uji_kompetensi.ba04', $data);
    }

    //MENU UMPAN BALI DAN CATATAN ASESMEN (AK 03)
    public function ubdcaak03(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Umpan Balik dan Catatan Asesmen (AK 03)";
        $token = $data_session['token'];
        return view('uji_kompetensi.ubdcaak03', $data);
    }
}
