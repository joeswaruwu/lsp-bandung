<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Asesor;
use App\Http\Controllers\Asesi;
use App\Http\Controllers\Penyelia;
use App\Http\Controllers\Alamat;
use App\Http\Controllers\Monitoring;
use App\Http\Controllers\MasterData;
use App\Http\Controllers\ManajemenJadwal;
use App\Http\Controllers\UjiKompetensi;

Route::controller(Auth::class)->group(function () {
    Route::get('/', 'login');
    Route::post('/validasiLogin', 'validasiLogin');
    Route::get('/logout', 'logout');
});

Route::controller(Admin::class)->group(function () {
    Route::get('/admin', 'admin');
    Route::get('/profile_lsp', 'profile_lsp');
    Route::post('/saveProfileLsp', 'saveProfileLsp');
});

Route::controller(Asesor::class)->group(function () {
    Route::get('/asesor', 'asesor');
    Route::get('/profile_asesor', 'profile_asesor');
    Route::post('/saveProfileAsesor', 'saveProfileAsesor');
});

Route::controller(Asesi::class)->group(function () {
    Route::get('/asesi', 'asesi');
    Route::get('/profile_asesi', 'profile_asesi');
    Route::post('/saveProfileAsesi', 'saveProfileAsesi');
});

Route::controller(Penyelia::class)->group(function () {
    Route::get('/penyelia', 'penyelia');
    Route::get('/profile_penyelia', 'profile_penyelia');
    Route::post('/saveProfilePenyelia', 'saveProfilePenyelia');
});

Route::controller(Alamat::class)->group(function () {
    Route::post('/getKabKota', 'getKabKota');
});

Route::controller(Monitoring::class)->group(function () {
    Route::get('/monitoring_apl01', 'monitoring_apl01');
});

Route::controller(MasterData::class)->group(function () {
    Route::get('/getSrukturJabatanLsp', 'getSrukturJabatanLsp');
    Route::get('/struktur_jabatan', 'struktur_jabatan');
    Route::post('/saveStrukturJabatanLsp', 'saveStrukturJabatanLsp');
    Route::post('/hapusStrukturJabatanLsp', 'hapusStrukturJabatanLsp');
    Route::post('/getDataStrukturJabatanLsp', 'getDataStrukturJabatanLsp');
    Route::post('/updateStrukturJabatanLsp', 'updateStrukturJabatanLsp');
    //daftar_admin
    Route::get('/daftar_admin', 'daftar_admin');
    Route::get('/getDaftarAdmin', 'getDaftarAdmin');
    Route::post('/saveDaftarAdmin', 'saveDaftarAdmin');
    Route::post('/hapusDaftarAdmin', 'hapusDaftarAdmin');
    Route::post('/getDataDaftarAdmin', 'getDataDaftarAdmin');
    Route::post('/updateDataDaftarAdmin', 'updateDataDaftarAdmin');
    //daftar_penyelia
    Route::get('/daftar_penyelia', 'daftar_penyelia');
    Route::get('/getDaftarPenyelia', 'getDaftarPenyelia');
    Route::post('/saveDaftarPenyelia', 'saveDaftarPenyelia');
    Route::post('/hapusDaftarPenyelia', 'hapusDaftarPenyelia');
    Route::post('/getDataDaftarPenyelia', 'getDataDaftarPenyelia');
    Route::post('/updateDataDaftarPenyelia', 'updateDataDaftarPenyelia');
});

Route::controller(ManajemenJadwal::class)->group(function () {
    Route::get('/jadwal_asesmen', 'jadwal_asesmen');
    Route::get('/getJadwalAsesmen', 'getJadwalAsesmen');
    Route::post('/getSubTuk', 'getSubTuk');
    Route::post('/getSubSkema', 'getSubSkema');
    Route::post('/saveJadwalAsesmen', 'saveJadwalAsesmen');
    Route::post('/hapusJadwalAsesmen', 'hapusJadwalAsesmen');
    Route::get('/jadwalLanjutTuk/{id}', 'jadwalLanjutTuk');

    Route::get('/penugasan_asesor', 'penugasan_asesor');
    Route::post('/getTanggalUjiJadwalAsesmen', 'getTanggalUjiJadwalAsesmen');
    Route::post('/getNoregAsesor', 'getNoregAsesor');
    Route::post('/saveJadwalAsesor', 'saveJadwalAsesor');

    Route::get('/surat_tugas', 'surat_tugas');
    Route::get('/getSuratTugas', 'getSuratTugas');
    Route::post('/saveSuratTugas', 'saveSuratTugas');
    Route::get('/view_surat_tugas/{id}', 'view_surat_tugas');
    Route::get('/getDataSuratTugas', 'getDataSuratTugas');
});

Route::controller(UjiKompetensi::class)->group(function () {
    Route::get('/pskapl01', 'pskapl01');
    Route::get('/getJadwalAsesmen_pskapl01', 'getJadwalAsesmen_pskapl01');
    Route::get('/jadwalLanjut/{id}', 'jadwalLanjut');
    Route::get('/apl01lsp/{idAsesor}/{idJadwalAsesmen}', 'apl01lsp');

    //MENU BANDING ASESMEN (AK 04)
    Route::get('/ba04', 'ba04');
    //MENU UMPAN BALI DAN CATATAN ASESMEN (AK 03)
    Route::get('/ubdcaak03', 'ubdcaak03');
});
