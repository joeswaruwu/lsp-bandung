$(document).ready(function () {
    datatabel = $("#tblPskapl01").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getJadwalAsesmen_pskapl01",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                        <a href="jadwalLanjut/${data.id}" class="btn btn-outline-primary btn-sm block" >Pilih Jadwal Asesmen
                        </a>
                    </div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_jadwal;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tuk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.id_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.target_peserta;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.jam_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.lokasi_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.sumber_anggaran;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.anggaran_perasesi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_mulai_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_akhir_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.durasi_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_instansi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_lsp;
                },
            },
        ],
    });
});

function reload_table(table) {
    table.ajax.reload();
}
