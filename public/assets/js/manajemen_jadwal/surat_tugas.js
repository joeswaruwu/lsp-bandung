$(document).ready(function () {
    datatabel = $("#tblSuratTugas").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getSuratTugas",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <a href="view_surat_tugas/${data.id}" class="btn btn-outline-warning btn-sm block" >
                   Lihat
                    </a>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_jadwal;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tuk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.sumber_anggaran;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_surat;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.lokasi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_keputusan;
                },
            },
        ],
    });
});

$("#nama_jadwal").change(function (event) {
    let id = document.getElementById("nama_jadwal").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getTanggalUjiJadwalAsesmen",
        type: "post",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.length == 1) {
                $("#skema").val(data[0].skema);
                $("#sumber_dana").val(data[0].sumber_anggaran);
                $("#nama_tuk").val(data[0].tuk);
                $("#nomor_spk").val(data[0].nomor_spk);
                $("#tanggal_spk").val(data[0].tanggal_spk);
            } else {
                $("#tanggal_uji").val("");
                $("#sumber_dana").val("");
                $("#nama_tuk").val("");
                $("#nomor_spk").val("");
                $("#tanggal_spk").val("");
            }
        },
    });
});
