$(document).ready(function () {
    datatabel = $("#tblJadwalAsesmen").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getJadwalAsesmen",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <a href="jadwalLanjutTuk/${data.id}" class="btn btn-outline-warning btn-sm block" >
                    <i class="bi bi-eye"></i>
                    </a>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                    <i class="bi bi-trash"></i>
                    </button>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_jadwal;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tuk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.id_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.target_peserta;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.jam_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.lokasi_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.sumber_anggaran;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.anggaran_perasesi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_mulai_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_akhir_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.durasi_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_instansi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_lsp;
                },
            },
        ],
    });
});

function hapus(id) {
    if (confirm("Hapus Jadwal Asesmen ?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusJadwalAsesmen",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

// function edit(id) {
//     $.ajax({
//         headers: {
//             "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//         },
//         url: "/getDataDaftarPenyelia",
//         type: "POST",
//         data: {
//             id: id,
//         },
//         dataType: "json",
//         success: function (data) {
//             console.log(data);
//             $("#id").val(id);
//             $("#email").val(data.dataPenyelia.email);
//             $("#password").val(data.dataPenyelia.password);
//             $("#nama").val(data.dataPenyelia.name);
//             $("#akses").val(data.dataPenyelia.role_id);
//             $("#alamat").val(data.dataPenyelia.alamat);
//             $("#kode_pos").val(data.dataPenyelia.kodepos);
//             $("#provinsi").val(data.dataPenyelia.provinsi);
//             let optionKabKota = `
//             <option value="${data.dataKabKota.kode}">${data.dataKabKota.nama}</option>
//             `;
//             $("#kab_kota").html(optionKabKota);

//             let buttonedit = `
//             <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
//                 Update
//             </button>
//             `;
//             $("#button_").html(buttonedit);

//             $("#btnedit").click(function (event) {
//                 event.preventDefault();

//                 let new_form = $("#form_crud")[0];
//                 let data = new FormData(new_form);

//                 $.ajax({
//                     url: "/updateDataDaftarPenyelia",
//                     enctype: "multipart/form-data",
//                     processData: false,
//                     contentType: false,
//                     cache: false,
//                     type: "POST",
//                     data: data,
//                     dataType: "json",
//                     success: function (data) {
//                         console.log(data);
//                         Swal.fire({
//                             icon: "success",
//                             title: "Berhasil",
//                             text: "Data Berhasil Diubah",
//                         });

//                         reload_table(datatabel);
//                         cancel();
//                     },
//                 });
//             });
//         },
//     });
// }

// function cancel() {
//     $("#default").modal("hide");
//     $("#id").val("");
//     $("#email").val("");
//     $("#password").val("");
//     $("#nama").val("");
//     $("#akses").val("");
//     $("#alamat").val("");
//     $("#kode_pos").val("");
//     $("#provinsi").val("");
//     $("#kab_kota").val("");

//     let buttontambah = `
//     <button type="submit" class="btn btn-primary me-1 mb-1">
//         Simpan
//     </button>
//                 `;
//     $("#button_").html(buttontambah);
// }

$("#tuk_kejuruan").change(function (event) {
    let valTukKejuruan = document.getElementById("tuk_kejuruan").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getSubTuk",
        type: "post",
        data: {
            valTukKejuruan: valTukKejuruan,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            let tuk = "";
            data.forEach((element) => {
                tuk += `
                <option value="${element.kode}">${element.nama}</option>
                `;
            });
            $("#tuk").html(tuk);
        },
    });
});

$("#skema_kejuruan").change(function (event) {
    let valSkemaKejuruan = document.getElementById("skema_kejuruan").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getSubSkema",
        type: "post",
        data: {
            valSkemaKejuruan: valSkemaKejuruan,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            let skema = "";
            data.forEach((element) => {
                skema += `
                <option value="${element.kode}">${element.nama}</option>
                `;
            });
            $("#skema").html(skema);
        },
    });
});
