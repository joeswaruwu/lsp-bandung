$("#nama_jadwal").change(function (event) {
    let id = document.getElementById("nama_jadwal").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getTanggalUjiJadwalAsesmen",
        type: "post",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.length == 1) {
                $("#tanggal_uji").val(data[0].tanggal_uji);
            } else {
                $("#tanggal_uji").val("");
            }
        },
    });
});

$("#asesor").change(function (event) {
    let id = document.getElementById("asesor").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getNoregAsesor",
        type: "post",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.length == 1) {
                $("#no_reg").val(data[0].no_reg);
            } else {
                $("#no_reg").val("");
            }
        },
    });
});
