$(document).ready(function () {
    // datatabel = $('#tblStrukturJabatanLsp').DataTable({
    //     dom: "Bfrtip"
    // });
    datatabel = $("#tblDaftarAdmin").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDaftarAdmin",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <button type="button" class="btn btn-outline-warning btn-sm block" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                    <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                    <i class="bi bi-trash"></i>
                    </button>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.jenis_lsp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.name;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.no_hp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.no_hp_kantor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.fax_kantor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.email;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.password;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.website;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.alamat;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.kodepos;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.no_sk_lsp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.dokumen_sk_lsp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.no_lisensi_lsp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.dokumen_lisensi_lsp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.npwp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.dokumen_npwp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.rekening_bank;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.dokumen_rek_bank;
                },
            },
        ],
    });
});

function hapus(id) {
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDaftarAdmin",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataDaftarAdmin",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#jenis_lsp").val(data[0].jenis_lsp);
            $("#nama").val(data[0].name);
            $("#no_hp").val(data[0].no_hp);
            $("#no_hp_kantor").val(data[0].no_hp_kantor);
            $("#no_fax").val(data[0].no_fax);
            $("#email").val(data[0].email);
            $("#password").val(data[0].password);
            $("#website").val(data[0].website);
            $("#kode_pos").val(data[0].kode_pos);
            $("#alamat_kantor").val(data[0].alamat_kantor);
            $("#no_sk_lsp").val(data[0].no_sk_lsp);
            if (data[0].dokumen_sk_lsp != "") {
                $("#riview_dokumen_sk_lsp").attr(
                    "src",
                    "assets/document/sk_lsp/" + data[0].dokumen_sk_lsp
                );
            } else {
                $("#riview_dokumen_sk_lsp").attr("src", "");
            }
            $("#no_lisensi_lsp").val(data[0].no_lisensi_lsp);
            if (data[0].dokumen_lisensi_lsp != "") {
                $("#riview_dokumen_lisensi_lsp").attr(
                    "src",
                    "assets/document/lisensi_lsp/" + data[0].dokumen_lisensi_lsp
                );
            } else {
                $("#riview_dokumen_lisensi_lsp").attr("src", "");
            }
            $("#npwp").val(data[0].npwp);
            if (data[0].dokumen_npwp != "") {
                $("#riview_dokumen_npwp").attr(
                    "src",
                    "assets/document/npwp/" + data[0].dokumen_npwp
                );
            } else {
                $("#riview_dokumen_npwp").attr("src", "");
            }
            $("#rekening_bank").val(data[0].rekening_bank);
            if (data[0].dokumen_rek_bank != "") {
                $("#riview_dokumen_rek_bank").attr(
                    "src",
                    "assets/document/rek_bank/" + data[0].dokumen_rek_bank
                );
            } else {
                $("#riview_dokumen_rek_bank").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataDaftarAdmin",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    $("#id").val("");
    $("#jenis_lsp").val("");
    $("#nama").val("");
    $("#no_hp").val("");
    $("#no_hp_kantor").val("");
    $("#no_fax").val("");
    $("#email").val("");
    $("#password").val("");
    $("#website").val("");
    $("#kode_pos").val("");
    $("#alamat_kantor").val("");
    $("#no_sk_lsp").val("");
    $("#dokumen_sk_lsp").val("");
    $("#riview_dokumen_sk_lsp").attr("src", "");
    $("#no_lisensi_lsp").val("");
    $("#dokumen_lisensi_lsp").val("");
    $("#riview_dokumen_lisensi_lsp").attr("src", "");
    $("#npwp").val("");
    $("#dokumen_npwp").val("");
    $("#riview_dokumen_npwp").attr("src", "");
    $("#rekening_bank").val("");
    $("#dokumen_rek_bank").val("");
    $("#riview_dokumen_rek_bank").attr("src", "");

    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
