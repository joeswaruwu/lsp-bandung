$("#provinsi").change(function (event) {
    let valProvinsi = document.getElementById("provinsi").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getKabKota",
        type: "post",
        data: {
            valProvinsi: valProvinsi,
            panjang: 5,
        },
        dataType: "json",
        success: function (data) {
            let valKabKota = "";
            data.forEach((element) => {
                valKabKota += `
                <option value="${element.kode}">${element.nama}</option>
                `;
            });
            $("#kab_kota").html(valKabKota);
        },
    });
});

$(document).ready(function () {
    // datatabel = $('#tblStrukturJabatanLsp').DataTable({
    //     dom: "Bfrtip"
    // });
    datatabel = $("#tblDaftarPenyelia").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDaftarPenyelia",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <button type="button" class="btn btn-outline-warning btn-sm block" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                    <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                    <i class="bi bi-trash"></i>
                    </button>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.email;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.password;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.name;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_role;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.alamat;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.kodepos;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_provinsi;
                },
            },
        ],
    });
});

function hapus(id) {
    alert(id);
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDaftarPenyelia",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataDaftarPenyelia",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#email").val(data.dataPenyelia.email);
            $("#password").val(data.dataPenyelia.password);
            $("#nama").val(data.dataPenyelia.name);
            $("#akses").val(data.dataPenyelia.role_id);
            $("#alamat").val(data.dataPenyelia.alamat);
            $("#kode_pos").val(data.dataPenyelia.kodepos);
            $("#provinsi").val(data.dataPenyelia.provinsi);
            let optionKabKota = `
            <option value="${data.dataKabKota.kode}">${data.dataKabKota.nama}</option>
            `;
            $("#kab_kota").html(optionKabKota);

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataDaftarPenyelia",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    $("#id").val("");
    $("#email").val("");
    $("#password").val("");
    $("#nama").val("");
    $("#akses").val("");
    $("#alamat").val("");
    $("#kode_pos").val("");
    $("#provinsi").val("");
    $("#kab_kota").val("");

    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
