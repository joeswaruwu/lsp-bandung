$(document).ready(function () {
    // datatabel = $('#tblStrukturJabatanLsp').DataTable({
    //     dom: "Bfrtip"
    // });
    datatabel = $("#tblStrukturJabatanLsp").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getSrukturJabatanLsp",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return data.nama_jabatan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nip;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.ttd;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return `

                    <button type="button" class="btn btn-outline-warning btn-sm block" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                        <i class="bi bi-pencil-square"></i> Edit
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                        <i class="bi bi-trash"></i> Hapus
                    </button>
                `;
                },
            },
        ],
    });
});

function hapus(id) {
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusStrukturJabatanLsp",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataStrukturJabatanLsp",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#jabatan").val(data[0].jabatan);
            $("#nama").val(data[0].nama);
            $("#nip").val(data[0].nip);
            if (data[0].ttd != "") {
                $("#riview_ttd").attr(
                    "src",
                    "assets/document/tanda_tangan/struktur_jabatan/" +
                        data[0].ttd
                );
            } else {
                $("#riview_ttd").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateStrukturJabatanLsp",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    $("#jabatan").val("");
    $("#nama").val("");
    $("#nip").val("");
    $("#riview_ttd").attr("src", "");
    $("#ttd").val("");
    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
