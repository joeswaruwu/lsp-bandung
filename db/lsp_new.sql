-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 06 Apr 2023 pada 23.21
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsp_new`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_menu`
--

CREATE TABLE `master_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_menu`
--

INSERT INTO `master_menu` (`id_menu`, `nama_menu`) VALUES
(1, 'LSP'),
(2, 'Uji Kompetensi'),
(3, 'Profile');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_sub_menu`
--

CREATE TABLE `master_sub_menu` (
  `id_master_sub_menu` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL,
  `icon` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_sub_menu`
--

INSERT INTO `master_sub_menu` (`id_master_sub_menu`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Master Data LSP', '', 'bi bi-folder2-open', 1),
(2, 1, 'Manajemen Jadwal', '', 'bi bi-journals', 1),
(3, 1, 'Monitoring', '', 'bi bi-clipboard-data', 1),
(4, 3, 'Profile LSP', 'profile_lsp', 'bi bi-person-lines-fill', 1),
(5, 2, 'Laporan', '', 'bi bi-person-lines-fill', 1),
(6, 3, 'Profile Asesor', 'profile_asesor', 'bi bi-person-lines-fill', 1),
(7, 2, 'Pra Asesmen', '', 'bi bi-person-lines-fill', 1),
(8, 3, 'Profile Asesi', 'profile_asesi', 'bi bi-person-lines-fill', 1),
(9, 3, 'Profile Penyelia', 'profile_penyelia', 'bi bi-person-lines-fill', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_sub_sub_menu`
--

CREATE TABLE `master_sub_sub_menu` (
  `id_master_sub_sub_menu` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `child_title` varchar(150) NOT NULL,
  `url_subsub_menu` varchar(150) NOT NULL,
  `is_aktif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `master_sub_sub_menu`
--

INSERT INTO `master_sub_sub_menu` (`id_master_sub_sub_menu`, `sub_menu_id`, `child_title`, `url_subsub_menu`, `is_aktif`) VALUES
(1, 1, 'Struktur Jabatan LSP', 'struktur_jabatan', 1),
(2, 1, 'Daftar Admin', 'daftar_admin', 1),
(3, 1, 'Daftar Penyelia', 'daftar_penyelia', 1),
(4, 2, 'Jadwal Asesmen Baru', 'jadwal_asesmen', 1),
(5, 2, 'Penugasan Asesor', 'penugasan_asesor', 1),
(6, 2, 'Surat Tugas', 'surat_tugas', 1),
(7, 3, 'Data APL 01', 'monitoring_apl01', 1),
(8, 3, 'Data APL 02', 'monitoring_apl02', 1),
(9, 3, 'Data AK 01', 'monitoring_ak01', 1),
(10, 3, 'Data AK 05', 'monitoring_ak05', 1),
(11, 3, 'Blanko', 'monitoring_blanko', 1),
(12, 3, 'Berita Acara Laporan Asesmen (AK 05)', 'balaak05', 1),
(13, 7, 'Permohonan Sertifikasi Kompetensi (APL 01)', 'pskapl01', 1),
(14, 7, 'Asesmen Mandiri (APL 02)', 'am02', 1),
(15, 7, 'Persetujuan Asesmen dan Kerahasiaan (AK 01)', 'padkak01', 1),
(16, 5, 'Umpan Balik dan Catatan Asesmen (AK 03)', 'ubdcaak03', 1),
(17, 5, 'Meninjau Proses Asesmen (AK 06)', 'mpaak06', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_users`
--

CREATE TABLE `master_users` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `akses` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `ket_tim_id` int(11) NOT NULL,
  `ket_tim_wilayah_id` int(11) NOT NULL,
  `token` varchar(300) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_users`
--

INSERT INTO `master_users` (`id`, `nama`, `email`, `pin`, `gambar`, `akses`, `role_id`, `ket_tim_id`, `ket_tim_wilayah_id`, `token`, `status`) VALUES
(1, 'adminpusat', 'adminpusat@gmail.com', '123', 'default.jpg', 'admin', 1, 0, 0, '928277331sds21', '1'),
(2, 'syamil', 'syamil@gmail.com', '123', 'default.jpg', 'member', 2, 0, 0, '21212dkdld31', '1'),
(3, 'Muhyiddien Rabbani A', 'muhyidienrabbani09@gmail.com', 'bismillah', 'default.jpg', 'tim_teknis', 3, 1, 14, '4595101', '1'),
(4, 'Bapak Reza', 'raldiansyah339@gmail.com', 'bismillah', 'default.jpg', 'tim_teknis', 3, 1, 14, '36685367', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_users_akses_menu`
--

CREATE TABLE `master_users_akses_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_users_akses_menu`
--

INSERT INTO `master_users_akses_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 3, 2),
(10, 2, 3),
(11, 3, 3),
(12, 4, 2),
(13, 4, 3),
(14, 5, 1),
(15, 5, 2),
(16, 5, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_users_akses_submenu`
--

CREATE TABLE `master_users_akses_submenu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `master_users_akses_submenu`
--

INSERT INTO `master_users_akses_submenu` (`id`, `role_id`, `sub_menu_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 3, 3),
(6, 3, 5),
(7, 3, 6),
(8, 4, 7),
(9, 4, 5),
(10, 4, 8),
(11, 5, 3),
(12, 5, 5),
(13, 5, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_users_akses_subsubmenu`
--

CREATE TABLE `master_users_akses_subsubmenu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `subsub_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `master_users_akses_subsubmenu`
--

INSERT INTO `master_users_akses_subsubmenu` (`id`, `role_id`, `subsub_menu_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 2, 5),
(6, 2, 6),
(13, 2, 7),
(14, 2, 8),
(15, 2, 9),
(16, 2, 10),
(17, 2, 11),
(18, 3, 7),
(19, 3, 10),
(20, 3, 12),
(21, 4, 13),
(22, 4, 14),
(23, 4, 15),
(24, 4, 16),
(25, 5, 7),
(26, 5, 8),
(27, 5, 9),
(28, 5, 10),
(29, 5, 17);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_users_role`
--

CREATE TABLE `master_users_role` (
  `id` int(11) NOT NULL,
  `nama_role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_users_role`
--

INSERT INTO `master_users_role` (`id`, `nama_role`) VALUES
(1, 'sa'),
(2, 'admin'),
(3, 'asesor'),
(4, 'asesi'),
(5, 'penyelia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_profile` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `foto_profile`, `alamat`, `tanggal_lahir`, `no_hp`, `jenis_kelamin`, `token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Admin', 'admin@gmail.com', NULL, '123', '', '', '', '', '', 'dk232ru12rjfir38393y47rh4', '2022-06-02 17:55:30', '2022-06-02 17:55:30', ''),
(2, 3, 'Asesor', 'asesor@gmail.com', NULL, '123', '', '', '', '', '', '83huhfj382fnvmd32y7e', '14 July 2022', '29 September 2022', ''),
(3, 4, 'Asesi', 'asesi@gmail.com', NULL, '123', '', '', '', '', '', 'sdfs3huhsd382fnvmd32sdf', '14 July 2022', '29 September 2022', ''),
(9, 5, 'Penyelia', 'penyelia@gmail.com', NULL, '123', '', '', '', '', '', 's323huhfj38sdfdsy7edf33', '14 July 2022', '29 September 2022', '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `master_menu`
--
ALTER TABLE `master_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `master_sub_menu`
--
ALTER TABLE `master_sub_menu`
  ADD PRIMARY KEY (`id_master_sub_menu`);

--
-- Indeks untuk tabel `master_sub_sub_menu`
--
ALTER TABLE `master_sub_sub_menu`
  ADD PRIMARY KEY (`id_master_sub_sub_menu`);

--
-- Indeks untuk tabel `master_users`
--
ALTER TABLE `master_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_users_akses_menu`
--
ALTER TABLE `master_users_akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_users_akses_submenu`
--
ALTER TABLE `master_users_akses_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_users_akses_subsubmenu`
--
ALTER TABLE `master_users_akses_subsubmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_users_role`
--
ALTER TABLE `master_users_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `master_menu`
--
ALTER TABLE `master_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `master_sub_menu`
--
ALTER TABLE `master_sub_menu`
  MODIFY `id_master_sub_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `master_sub_sub_menu`
--
ALTER TABLE `master_sub_sub_menu`
  MODIFY `id_master_sub_sub_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `master_users`
--
ALTER TABLE `master_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `master_users_akses_menu`
--
ALTER TABLE `master_users_akses_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `master_users_akses_submenu`
--
ALTER TABLE `master_users_akses_submenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `master_users_akses_subsubmenu`
--
ALTER TABLE `master_users_akses_subsubmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `master_users_role`
--
ALTER TABLE `master_users_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
